"""
This class checks the correctness of Berkeley Algorithm
"""

import win32api
import threading
import socket
import time

from datetime import datetime
from PythonBerkeley.server_berkeley import HEADERSIZE, parser, DATA_LENGTH


class ClientBerkeley:
    def __init__(self, port=2999, host='192.168.1.4'):
        self.host = host
        self.port = port

    # main function of class
    def client_connection(self):
        try:
            # create client socket
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # connect to server
            clientSocket.connect((self.host, self.port))
            th1 = threading.Thread(target=self.send_time, args=(clientSocket,))
            th1.start()
            th2 = threading.Thread(target=self.recive_time, args=(clientSocket,))
            th2.start()
        except Exception as e:
            print("Something goes wrong : ", e)

    # function to send time to the server
    @staticmethod
    def send_time(client_socket):
        while True:
            try:
                client_socket.sendall((f'{len(str(datetime.now())):<{HEADERSIZE}}' + f'{datetime.now()}').encode())
                print("SEND_SERVER", datetime.now())
                print("\n")
            except Exception as err:
                print("Eroare[Client_Berkeley]- send_time_function", err)
                # os._exit(1)
            time.sleep(1)

    # function for reciving server time :
    def recive_time(self, client_socket):
        message_length = 0
        try:
            while True:
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != message_length:
                    # recive data from server
                    msg = client_socket.recv(DATA_LENGTH)
                    if new_msg:
                        message_length = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                sync_time = parser.parse(full_msg[HEADERSIZE:])
                # set the calculated time
                print("RECIVE_SERVER = ", sync_time)
                print("ACTUAL_CLIENT =", datetime.now())
                print("DIFF", (datetime.now() - sync_time))
                self.set_time(sync_time)
                print("ACTUAL_AFTER_RECIVING ", datetime.now())
                print("\n")
        except (ConnectionResetError, ConnectionRefusedError):
            print("[ERORR]Connection is Refuset/Reset")
            client_socket.close()

    # update SystemTime on server
    @staticmethod
    def set_time(time_to_set):
        updated_time = time_to_set
        real_time = win32api.GetSystemTime()
        win32api.SetSystemTime(real_time[0], real_time[1], real_time[2], real_time[3],
                               real_time[1], updated_time.minute, updated_time.second,
                               int(round(updated_time.microsecond // 1000)))


if __name__ == "__main__":
    ClientBerkeley().client_connection()
