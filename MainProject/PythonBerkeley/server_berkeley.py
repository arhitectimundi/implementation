"""
Server-Side of Berkeley Algorithm running on OrangePi Zero
"""

import threading
import traceback
import win32api
import socket
import time
import sys
import os
from datetime import datetime, timedelta
from dateutil import parser
from pathlib import Path
from IServer import IServer

# CONSTANTS
HEADERSIZE = 10
WORD = 16
# end reception of data after 12 recived result by 6 for each devices
END_VALUE = 12

# dictionary which store TOF(time of fligth) for connected devices
# RASP means raspberry pi and ORANGE means orange pi
# SAME - client and server are connected to the same WLAN
# NSAME - client and server are connected not in the same WLAN
# Wx - indicate the WLAN number
TOF_DICT = {"TOF_SAME_RASP_W1": "0:00:00.005496",
            "TOF_NSAME_RASP_W2": "0:00:00.007116",
            "TOF_SAME_RASP_W2": "0:00:00.004712",
            "TOF_NSAME_RASP_W1": "0:00:00.006064",
            "TOF_SAME_ORANGE_W1": "0:00:00.006091",
            "TOF_NSAME_ORANGE_W1": "0:00:00.008613"
            }

PATH_TWR = "C:\\Users\\iongu\\Desktop\\MainProject\\SavedData\\twr_result.txt"


class ServerBerkeley(IServer):
    # constructor of Server
    def __init__(self, port=2989, client_data=None):
        if client_data is None:
            client_data = {}
        self.client_data = client_data
        self.port = port
        self.max_save_data = 0
        self.data = self.read_data_from_file(PATH_TWR)

    # main function of a server
    def start_master_server(self):
        try:
            # create server socket
            server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # set option of reuse adress
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # bind on specific port
            server_socket.bind(('', self.port))
            server_socket.listen(10)
            print("Listening...")
            th1 = threading.Thread(target=self.handle_connection, args=(server_socket,))
            th1.start()
            th2 = threading.Thread(target=self.send_data, args=())
            th2.start()
            th2.join()
        except socket.error as err:
            print("Create Socket server  Fail with error ", err)
            sys.exit(1)

    # function to handle connection of server
    def handle_connection(self, master_server):
        while True:
            client_connection, addr = master_server.accept()
            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            try:
                thread = threading.Thread(target=self.recive_client_data, args=(client_connection, addr,))
                thread.start()
            except Exception as e:
                print("Cannot start recive thread")
                traceback.print_exc()

    # recive the data from the clients and save in a dictionary where key = client_adress,
    # data =  client_time,difference between client and master time,and client connection
    def recive_client_data(self, client_connection, client_adress):
        message_length = 0
        try:
            while True:
                self.max_save_data += 1
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != message_length:
                    # se receptioneaza datele de la server
                    msg = client_connection.recv(WORD)
                    if new_msg:
                        message_length = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                if self.max_save_data <= END_VALUE:
                    client_time = parser.parse(full_msg[HEADERSIZE:])
                    if self.data[0].split(",")[0] == client_adress[0]:
                        time_of_fligth = datetime.strptime(self.data[0][:-1].split(",")[1], '%H:%M:%S.%f')
                    elif self.data[1].split(",")[0] == client_adress[0]:
                        time_of_fligth = datetime.strptime(self.data[1][:-1].split(",")[1], '%H:%M:%S.%f')
                    print("RECIVE_CLIENT " + client_adress[0] + " " + str(client_time))
                    print("SERVER_TIME" + client_adress[0] + " " + str(datetime.now()))
                    # calculate difference in time between server and client
                    # also added correction with time of fligth
                    master_slave_time_diff = datetime.now() - (client_time +
                                                               timedelta(
                                                                   milliseconds=time_of_fligth.microsecond / 1000))
                    print("DIFF " + client_adress[0] + " = " + str(master_slave_time_diff))
                    print("\n")
                    # structure that saves client info.
                    self.client_data[client_adress] = {
                        "client_time": client_time,
                        "master_client_diff": master_slave_time_diff,
                        "client_connection": client_connection
                    }

                elif self.max_save_data >= END_VALUE:
                    print("Aici")
                    # after reciving data close all existing connection and exit server
                    for _, clien_conn in self.client_data.items():
                        clien_conn.close()
                    os._exit(1)
        except Exception as e:
            # When one of this errors occurs
            # connection should be closed

            if len(self.client_data):
                self.client_data.pop(client_adress)
            client_connection.close()
            if not self.client_data:
                os._exit(1)

    @staticmethod
    def read_data_from_file(file_name):
        myFile = Path(file_name)
        print(myFile.is_file())
        if myFile.is_file():
            with open(file_name, 'r+') as file:
                lines = file.readlines()
                file.truncate(0)
                return lines

    # syncronize the client clocks
    def send_data(self):
        while True:
            if len(self.client_data) > 0:
                # calculate the average difference between clients cloks
                average_clock_diff = self.calculate_average_clock_diff()
                for _, client_connection in self.client_data.items():
                    try:
                        sync_time = datetime.now() + average_clock_diff
                        # send syncronized time to all devices
                        try:
                            client_connection["client_connection"].sendall(
                                (f'{len(str(sync_time)):<{HEADERSIZE}}' + f'{sync_time}').encode())
                        except Exception:
                            print("Client was already deleted, can not send data, exit")
                            for adress, clients in list(self.client_data.items()):
                                if clients["client_connection"] == client_connection["client_connection"]:
                                    self.client_data.pop(adress)
                            os._exit(1)
                        # change server time:
                        self.set_time(sync_time)
                    except Exception as e:
                        print("Something goes wrong " + e)
            time.sleep(1)

    # calculating the average time difference between clients time and server time
    def calculate_average_clock_diff(self):
        data_list = list(client_connection["master_client_diff"]
                         for _, client_connection in self.client_data.items())
        average_clock_diff = (sum(data_list, timedelta(0, 0))) / len(self.client_data)
        print("AVERAGE_DIFF", average_clock_diff)
        return average_clock_diff

    # Set server time, the time is updated up to hour.
    # is unnecessary to update time if difference is too big
    @staticmethod
    def set_time(time_to_set):
        updated_time = time_to_set
        real_time = win32api.GetSystemTime()
        win32api.SetSystemTime(real_time[0], real_time[1], real_time[2], real_time[3],
                               real_time[4], updated_time.minute, updated_time.second,
                               int(round(updated_time.microsecond // 1000)))


if __name__ == "__main__":
    a = ServerBerkeley()
    a.start_master_server()
