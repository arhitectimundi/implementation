"""
Client-Side of Berkeley Algorithm running on OrangePi Zero
"""

import threading
import socket
import time
import os
from datetime import datetime
from dateutil import parser
import subprocess
import shlex

# CONSTANTS
HEADERSIZE = 10
DATA_LENGTH = 16


class ClientBerkeley:
    def __init__(self, port=2999, host='192.168.1.4'):
        self.host = host
        self.port = port

    # main function of class
    def client_connection(self):
        try:
            # create client socket
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # connect to server
            clientSocket.connect((self.host, self.port))
            th1 = threading.Thread(target=self.send_time, args=(clientSocket,))
            th1.start()
            th2 = threading.Thread(target=self.recive_time, args=(clientSocket,))
            th2.start()
            th2.join()
        except Exception as e:
            print("Something goes wrong : ", e)


    # this function send actual time to the server
    @staticmethod
    def send_time(client_socket):
        while True:
            try:
                client_socket.send((f'{len(str(datetime.now())):<{HEADERSIZE}}' + f'{datetime.now()}').encode())
                print("SEND_SERVER_TIME", datetime.now())
                print("\n")
            except Exception as err:
                print("Erorr[Client_Berkeley]- send_time_function", err)
                os._exit(1)
            time.sleep(1)

    # function for reciving server time :
    @staticmethod
    def recive_time(client_socket):
        message_length = 0
        try:
            # deactivate ntp update of time
            subprocess.call(shlex.split("sudo timedatectl set-ntp false"))
            while True:
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != message_length:
                    # recive data from server
                    msg = client_socket.recv(DATA_LENGTH)
                    if new_msg:
                        message_length = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                sync_time = parser.parse(full_msg[HEADERSIZE:])

                print("RECIVE_SERVER_TIME = ", sync_time)
                print("ACTUAL_CLIENT_TIME =", datetime.now())
                print("DIFF ", abs(datetime.now() - sync_time))
                # set calculated time via a  sequential subprocess
                subprocess.Popen(shlex.split("sudo date -s '%s'" % sync_time), stdout=subprocess.PIPE).communicate()
                print("ACTUAL_TIME_AFTER_RECIVING ", datetime.now())
                print("\n")
        except (ConnectionResetError, ConnectionRefusedError):
            print("Conexion refused/reset")
            client_socket.close()
            os._exit(1)


if __name__ == "__main__":
    ClientBerkeley().client_connection()
