"""
Client-Side of Cristian Algorithm running on OrangePi Zero
"""

import subprocess
import os
import shlex
import socket
import threading
import time
from datetime import timedelta, datetime
from dateutil import parser
from timeit import default_timer
# Import Orange Pi GPIO library
from pyA20.gpio import gpio
from pyA20.gpio import port

# CONSTANTS

GPIO_NUM = port.PA6
HEADERSIZE = 10
MAX_NUM_OF_INTERUPTS = 5
WORD = 16


class ClientChristian:
    def __init__(self, port=2997, host='192.168.1.4'):
        self.port = port
        self.host = host
        self.calculated_client_time = 0
        self.final_data = list()
        self.num_of_interrupts = 0

    def start_client(self):
        try:
            # create a client socket
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # connected to server interface
            client_socket.connect((self.host, self.port))
            # create thread for send/recive data
            send_thread = threading.Thread(target=self.send_data, args=(client_socket,))
            send_thread.start()
            recive_thread = threading.Thread(target=self.recive_data, args=(client_socket,))
            recive_thread.start()
        except socket.error as err:
            print("Create Socket client fail with error ", err)

    # callback function for button
    # this function store the callculated time of client
    def button_callback(self):
        if not len(self.final_data):
            self.final_data.clear()
        if self.calculated_client_time:
            self.final_data.append(datetime.now())
        # the reciving of data wil be stoped after 5 interrupts
        self.num_of_interrupts += 1

    def button_settings(self):
        # set a initial state for button
        initial_button_state = True
        # initiate gpio libraty
        gpio.init()
        # set pin as input
        gpio.setcfg(GPIO_NUM, gpio.INPUT)
        # set pin with pullup
        gpio.pullup(GPIO_NUM, gpio.PULLUP)
        while True:
            # read the button state and if button was pressed
            # callback function is invoked
            current_button_state = gpio.input(GPIO_NUM)
            if current_button_state == initial_button_state:
                self.button_callback()
            # a bounce time
            time.sleep(0.200)

    def recive_data(self, client_server):
        # start the button routine
        threading.Thread(target=self.button_settings).start()
        # deactivate ntp update
        subprocess.call(shlex.split("sudo timedatectl set-ntp false"))
        mslen = 0
        try:
            while True:
                full_msg = ''
                new_msg = True
                # defaul_timer is a timer of high precision
                time_of_request = default_timer()
                while len(full_msg) - HEADERSIZE != mslen:
                    # recive data from server
                    msg = client_server.recv(16)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                server_time = parser.parse(full_msg[HEADERSIZE:])
                # calculate time of reciving response
                time_of_response = default_timer()
                round_trip_time = time_of_response - time_of_request
                # calculate client clock time
                self.calculated_client_time = server_time + timedelta(seconds=round_trip_time / 2)
                # set caluleted tiem as time of device
                subprocess.call(shlex.split("sudo date -s '%s'" % self.calculated_client_time))
                # calculate error betwen server and client
                error = abs(server_time - self.calculated_client_time)
                print("Server_Time ", server_time)
                print("Error betwween server and client is ", error)
                print("\n\n")
        except (ConnectionResetError, ConnectionRefusedError):
            print("Connection is refused/reset! ")
            client_server.close()
            os._exit(1)

    def send_data(self, socket_client):
        while True:
            if len(self.final_data):
                try:
                    print("DATA_SEND")
                    data = self.final_data.pop()
                    socket_client.sendall((f'{len(str(data)):<{HEADERSIZE}}' + f'{data}').encode())
                    if self.num_of_interrupts >= MAX_NUM_OF_INTERUPTS:
                        print("YOU PRESSED BUTTON for 5 time ")
                        socket_client.close()
                        os._exit(1)
                except Exception as err:
                    print('Erorr occurs' + str(err))
            time.sleep(1)


if __name__ == "__main__":
    ClientChristian().start_client()
