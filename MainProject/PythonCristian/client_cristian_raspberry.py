"""
Client-Side of Cristian Algorithm running on Raspberry Pi
"""
import os
import subprocess
import shlex
import socket
import threading
import time
from datetime import timedelta,datetime
from dateutil import parser
from timeit import default_timer

# Import Raspberry Pi GPIO library
# import RPi.GPIO as GPIO

GPIO_NUM = 8
HEADERSIZE = 10
MAX_NUM_OF_INTERUPTS = 5
WORD = 16


class ClientChristian:
    def __init__(self, port=2997, host='192.168.1.4'):
        self.port = port
        self.host = host
        self.calculated_client_time = 0
        self.final_data = list()
        self.num_of_interrupts = 0

    def start_client(self):
        try:
            # create a socket for connection with server
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # connect to socket interface
            clientSocket.connect((self.host, self.port))
            # recive/send data from/to server
            send_thread = threading.Thread(target=self.send_data, args=(clientSocket,))
            send_thread.start()
            recive_thread = threading.Thread(target=self.recive_data, args=(clientSocket,))
            recive_thread.start()
        except socket.error as err:
            print("Create Socket client fail with error ", err)

    # callback function for button
    # this function store the callculated time of client
    def button_callback(self, _):
        if not len(self.final_data):
            self.final_data.clear()
        if self.calculated_client_time:
            self.final_data.append(datetime.now())
        self.num_of_interrupts += 1

    def button_settings(self):
        # ignor warnings
        GPIO.setwarnings(False)
        # use board numerotation
        GPIO.setmode(GPIO.BOARD)
        # GPIO pin is set as input pull_up
        GPIO.setup(GPIO_NUM, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # set call_back function with a bounce time of 200 ms
        GPIO.add_event_detect(GPIO_NUM, GPIO.RISING, callback=self.button_callback, bouncetime=200)

    def recive_data(self, client_socket):
        # set button settings
        self.button_settings()
        # deactivate ntp update
        subprocess.call(shlex.split("sudo timedatectl set-ntp false"))
        message_length = 0
        try:
            while True:
                full_msg = ''
                new_msg = True
                # default timer is a higth precision timer
                time_of_request = default_timer()
                while len(full_msg) - HEADERSIZE != message_length:
                    # recive data from server
                    msg = client_socket.recv(WORD)
                    if new_msg:
                        message_length = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                server_time = parser.parse(full_msg[HEADERSIZE:])
                # calculate time of reciving response
                time_of_response = default_timer()
                round_trip_time = time_of_response - time_of_request
                # calculate client time
                self.calculated_client_time = server_time + timedelta(seconds=round_trip_time / 2)
                # set client time to device
                subprocess.call(shlex.split("sudo date -s '%s'" % self.calculated_client_time))
                # calculate error betwen server and client
                error = abs(server_time - self.calculated_client_time)

                print("Server_Time ", server_time)
                print("Error betwween server and client is ", error)
                print("\n\n")
        except (ConnectionResetError, ConnectionRefusedError):
            print("Connection is refused/reset! ")
            client_socket.close()
            os._exit(1)

    def send_data(self, socket_client):
        while True:
            if len(self.final_data):
                try:
                    print("DATA_SEND")
                    data = self.final_data.pop()
                    socket_client.sendall((f'{len(str(data)):<{HEADERSIZE}}' + f'{data}').encode())
                    if self.num_of_interrupts >= MAX_NUM_OF_INTERUPTS:
                        print("YOU PRESSED BUTTON for 5 time ")
                        socket_client.close()
                        os._exit(1)
                except Exception as err:
                    print('Errors occured' + str(err))
            time.sleep(1)


if __name__ == "__main__":
    ClientChristian().start_client()
