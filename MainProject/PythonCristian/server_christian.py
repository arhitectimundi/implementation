"""
Server-Side of Cristian Algorithm running on OrangePi Zero
"""

import socket
import time
import sys
import os
import traceback
from datetime import datetime, timedelta
from threading import Thread
from pathlib import Path
from dateutil import parser
from IServer import IServer

HEADERSIZE = 10
WORD = 16
# dictionary which store TOF(time of fligth) for connected devices
# RASP means raspberry pi and ORANGE means orange pi
# SAME - client and server are connected to the same WLAN
# NSAME - client and server are connected not in the same WLAN
# Wx - indicate the WLAN number
TOF_DICT = {"TOF_SAME_RASP_W1": "0:00:00.005496",
            "TOF_NSAME_RASP_W2": "0:00:00.007116",
            "TOF_SAME_RASP_W2": "0:00:00.004712",
            "TOF_NSAME_RASP_W1": "0:00:00.006064",
            "TOF_SAME_ORANGE_W1": "0:00:00.006091",
            "TOF_NSAME_ORANGE_W1": "0:00:00.008613"
            }
PATH_TWR = "C:\\Users\\iongu\\Desktop\\MainProject\\SavedData\\twr_result.txt"


class ServerCristian(IServer):

    def __init__(self, connected_list=None, port=2997):
        if connected_list is None:
            connected_list = {}
        self.connected_list = connected_list
        self.port = port
        self.server_time = 0
        self.data = self.read_data_from_file(PATH_TWR)

    def start_master_server(self):
        try:
            # create server socket
            server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # server bind for all interfaces
            server_socket.bind(('', self.port))
            # se vor accepta max. 10 conexiuni, ele fiind indeajuns pentru aplicatia data
            server_socket.listen(10)
            print("Listening...")
            # two main thread are started for send/recive data
            handle_connection_thread = Thread(target=self.handle_connection, args=(server_socket,))
            handle_connection_thread.start()
            send_clock_thread = Thread(target=self.send_data, args=())
            send_clock_thread.start()
        except socket.error as err:
            print("Create Socket server  Fail with error ", err)
            sys.exit(1)

    # function that handle connection with clients
    def handle_connection(self, server_connection):
        while True:
            # accept connection with client
            client_connection, addr = server_connection.accept()
            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            try:
                # try recive the client data
                recive_thread = Thread(target=self.recive_client_data, args=(client_connection, addr,))
                recive_thread.start()
            except Exception as e:
                print("Thread did not start.")
                traceback.print_exc()

    def recive_client_data(self, client_connection, client_adress):
        message_length = 0
        try:
            while True:
                # the connection with client is stored in a dictionary
                # where client address are key and connection is value
                self.connected_list[client_adress] = client_connection
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != message_length:
                    # recive client data
                    msg = client_connection.recv(WORD)
                    if new_msg:
                        message_length = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                recived_message = parser.parse(full_msg[HEADERSIZE:])
                actual_time = datetime.now()
                # the TOF is specified for each devises manually
                if self.data[0].split(",")[0] == client_adress[0]:
                    time_of_fligth = datetime.strptime(self.data[0][:-1].split(",")[1], '%H:%M:%S.%f')
                elif self.data[1].split(",")[0] == client_adress[0]:
                    time_of_fligth = datetime.strptime(self.data[1][:-1].split(",")[1], '%H:%M:%S.%f')

                # calculate difference between client and server
                # the difference is made with tof - update
                difference = abs(actual_time - (recived_message +
                                                timedelta(milliseconds=time_of_fligth.microsecond / 1000)))
                concatenated_data = str(recived_message) + "," + str(actual_time) + "," + str(difference)
                # create a thread for saving algorithm data
                Thread(target=self.write_to_file, args=(concatenated_data, str(client_adress[0]))).start()

        except (ConnectionResetError, ConnectionAbortedError, ValueError, KeyError, TimeoutError):
            # when a error accurs client_connection is closed()
            self.connected_list.pop(client_adress)
            client_connection.close()
            if not self.connected_list:
                os._exit(1)

    # write data to file
    @staticmethod
    def write_to_file(data, file_name):
        file = open("C:\\Users\\iongu\\Desktop\\MainProject\\SavedData\\" + file_name + ".txt", "a+")
        file.write(data)
        file.write("\n")
        file.close()

    @staticmethod
    def read_data_from_file(file_name):
        myFile = Path(file_name)
        print(myFile.is_file())
        if myFile.is_file():
            with open(file_name, 'r+') as file:
                lines = file.readlines()
                file.truncate(0)
                return lines

    def send_data(self):
        # send response to client
        while True:
            if len(self.connected_list) > 0:
                for client_connection in self.connected_list:
                    try:
                        # send current time to server
                        self.connected_list[client_connection].sendall(
                            (f'{len(str(datetime.now())):<{HEADERSIZE}}' + f'{datetime.now()}').encode())
                    except Exception as e:
                        print("Something goes wrong ", e)

            # set time send interval = 1 s
            time.sleep(1)


if __name__ == "__main__":
    a = ServerCristian()
    a.start_master_server()
