"""
This class is used to find all connected device on a specific interface
"""

import nmap
import socket


class IpScanner(object):
    def __init__(self, ip):
        self.ip = ip

    def scan_adress(self):
        dictionary = {}
        if len(self.ip) == 0:
            my_adress = '192.168.1.1/24'
        else:
            my_adress = self.ip + '/24'
        # create a network map to store host
        networkmap = nmap.PortScanner()

        # ping scan disable, activate port scan
        # save in a dictionary host(IP)- key and hostName- value
        networkmap.scan(hosts=my_adress, arguments='-sn')
        for host in networkmap.all_hosts():
            # calculate all
            dictionary[host] = socket.getfqdn(host)
        return dictionary
