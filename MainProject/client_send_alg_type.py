import socket

"""
This class is Client_Side use for sending to connected devices necessary algorithm to run
"""
HEADERSIZE = 10


class Client:
    def __init__(self, port=2993, host='localhost'):
        self.port = port
        self.host = host

    # send data, which is type of algorithm to devices
    @staticmethod
    def send_data(socket_client, data_to_send):
        try:
            socket_client.send((f'{len(str(data_to_send)):<{HEADERSIZE}}' + f'{data_to_send}').encode())
        except Exception as e:
            print("Something goes wrong ", e)

    def start_clientul(self, data_to_send):
        # create socket
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # connect to server
        clientSocket.connect((self.host, self.port))
        # send data to server
        self.send_data(clientSocket, data_to_send)
        # after data are sending the connection is close
        clientSocket.close()
