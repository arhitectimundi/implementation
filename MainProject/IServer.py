"""
Interface class that is a abstract base class
that provides inheriting to server implementation
of algorithms
"""
from abc import ABC, abstractmethod


class IServer(ABC):
    @abstractmethod
    def start_master_server(self):
        pass

    @abstractmethod
    def handle_connection(self, master_server):
        pass

    @abstractmethod
    def send_data(self):
        pass

    @abstractmethod
    def recive_client_data(self, client_connection, client_adress):
        pass
