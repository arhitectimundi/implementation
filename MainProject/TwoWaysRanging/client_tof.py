"""
This class implement Client_Side for Two way Ranging
"""
import socket
import datetime
import time
from dataclasses import dataclass
import threading
import os

HEADERSIZE = 10
WORD = 16
CLIENT_INITIAL_SENT = 'Csi'
CLIENT_FINAL_SENT = 'Csf'
SERVER_INITIAL_SENT = 'Ssi'


# class used as a structure to store necessary data
@dataclass
class TimePackageClient:
    time_send_init: datetime = 0
    time_recv_init: datetime = 0
    time_send_final: datetime = 0


class ClientTof:
    def __init__(self, my_package, port=2997, host='192.168.1.2'):
        self.host = host
        self.port = port
        self.my_package = my_package

    # function to send data to server
    def send_time(self, client_socket):
        flag = True
        try:
            while True:
                # save time when first data is sent
                if self.my_package.time_send_init == 0:
                    self.my_package.time_send_init = datetime.datetime.now()
                    client_socket.sendall((f'{len(str(CLIENT_INITIAL_SENT)):<{HEADERSIZE}}' +
                                           f'{CLIENT_INITIAL_SENT}').encode())
                    # if package from server is
                elif self.my_package.time_recv_init and flag:
                    self.my_package.time_send_final = datetime.datetime.now()
                    client_socket.sendall(
                        (f'{len(str(CLIENT_FINAL_SENT)):<{HEADERSIZE}}' + f'{CLIENT_FINAL_SENT}').encode())
                    flag = False
                    # after data was send are calculated time between send/recive.
                    # data are send concatenated
                elif self.my_package.time_recv_init and self.my_package.time_send_final:
                    round_trip_time1 = abs(self.my_package.time_recv_init - self.my_package.time_send_init)
                    round_trip_time2 = abs(self.my_package.time_send_final - self.my_package.time_recv_init)
                    # add id's for decode message correct
                    data_send = 'i1' + str(round_trip_time1) + 'f1' + 'i2' + str(round_trip_time2) + 'f2'
                    client_socket.sendall((f'{len(str(data_send)):<{HEADERSIZE}}' + f'{data_send}').encode())
                    self.my_package = 0
                    time.sleep(1)
                    client_socket.close()
                    os._exit(1)
        except AttributeError as e:
            print(e)

    def recive_time(self, client_socket):
        mslen = 0
        try:
            while True:
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != mslen:
                    # recive client data, all data send in aplication are sent in 2 package
                    # this is why TOF is not divided by 2
                    msg = client_socket.recv(WORD)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                recive_data = full_msg[HEADERSIZE:]
                if str(SERVER_INITIAL_SENT) in recive_data:
                    self.my_package.time_recv_init = datetime.datetime.now()
        except (ConnectionResetError, ConnectionRefusedError):
            print("Connection refused/reset! ")
            client_socket.close()
            os._exit(1)

    # main function
    def client_connection(self):
        try:
            # create socket
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # connect to server
            clientSocket.connect((self.host, self.port))
            # send/recive thread
            t1 = threading.Thread(target=self.send_time, args=(clientSocket,))
            t1.start()
            t2 = threading.Thread(target=self.recive_time, args=(clientSocket,))
            t2.start()
        except Exception as e:
            print("Something goes wrong client : ", e)


if __name__ == "__main__":
    my_pack = TimePackageClient()
    client_server = ClientTof(my_pack)
    client_server.client_connection()
