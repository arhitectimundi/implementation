"""
This class implement Server_Side for Two way Ranging
"""
from dataclasses import dataclass
import socket
import threading
from datetime import datetime
import time
import os

# CONSTANTS
HEADERSIZE = 10
WORD = 16
SERVER_INITIAL_SENT = 'Ssi'
CLIENT_INITIAL_SENT = 'Csi'
CLIENT_FINAL_SENT = 'Csf'


# class to save data necessary for algorithm
@dataclass
class TimePackageServer:
    time_recv_init: datetime = 0
    time_send: datetime = 0
    time_recv_final: datetime = 0
    round_trip_time_client1: datetime = 0
    round_trip_time_client2: datetime = 0
    round_trip_time_server1: datetime = 0
    round_trip_time_server2: datetime = 0


class ServerTof:
    def __init__(self, my_package, port=2997, client_data=None):
        if client_data is None:
            client_data = {}
        self.client_data = client_data
        self.port = port
        self.my_package = my_package
        self.connected_client = 0

    # are recived data from client
    # clients are stored in a dictionary, for a easiest access
    def recive_client_data(self, client_connection, client_adress):
        mslen = 0
        try:
            self.client_data[client_adress] = client_connection
            while True:
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != mslen:
                    # recive data from server
                    msg = client_connection.recv(WORD)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                recive_data = full_msg[HEADERSIZE:]
                # save time when first package is recived form client
                if recive_data == str(CLIENT_INITIAL_SENT):
                    self.my_package.time_recv_init = datetime.now()
                # save time when second package is recived form client
                elif recive_data == str(CLIENT_FINAL_SENT):
                    self.my_package.time_recv_final = datetime.now()
                else:
                    # get round trip time from client side
                    if self.find_between(recive_data, "i1", "f1"):
                        self.my_package.round_trip_time_client1 = self.find_between(recive_data, "i1", "f1")
                    if self.find_between(recive_data, "i2", "f2"):
                        self.my_package.round_trip_time_client2 = self.find_between(recive_data, "i2", "f2")
                    # if all data are recived
                    # calculate time of fligth using two ways ranging algotirhm
                    if self.my_package.time_recv_final != 0:
                        self.my_package.round_trip_time_server2 = abs(
                            self.my_package.time_recv_final - self.my_package.time_send)
                        self.my_package.round_trip_time_server1 = abs(
                            self.my_package.time_send - self.my_package.time_recv_init)
                        TOF = self.calculate_time_of_fligth(self.my_package.round_trip_time_client1,
                                                            self.my_package.round_trip_time_client2,
                                                            self.my_package.round_trip_time_server2,
                                                            self.my_package.round_trip_time_server1)
                        # print the result in console
                        data = str(client_adress[0]) + "," + str(abs(TOF))
                        self.write_to_file(data, "twr_result")
                        self.my_package = TimePackageServer()

                        for key in list(self.client_data):
                            if client_adress == key:
                                self.client_data.pop(key)
                                client_connection.close()
                                self.connected_client += 1
                                print("Am sters")

                        # print(len(self.client_data))
                        if self.connected_client >= 2:
                            os._exit(1)
                        break

                        # os._exit(1)
        except Exception as e:
            for key in list(self.client_data):
                if client_adress == key:
                    self.client_data.pop(key)
                    client_connection.close()
                    print("Am sterss")
                    os._exit(1)
            # if a error accurs
            # connecton with client is closed
            print(e)

    @staticmethod
    def write_to_file(data, file_name):
        file = open("C:\\Users\\iongu\\Desktop\\MainProject\\SavedData\\" + file_name + ".txt", "a+")
        file.write(data)
        file.write("\n")
        file.close()

    # handle client connection
    def handle_connections(self, master_server):
        while True:
            client_connection, addr = master_server.accept()
            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            # recive client data
            t3 = threading.Thread(target=self.recive_client_data, args=(client_connection, addr,))
            t3.start()
            t3.join()

    def send_data(self):
        try:
            flag = False
            while True:
                if len(self.client_data) > 0:
                    for client_connection in list(self.client_data):
                        # send data only if recived first package
                        if not self.my_package.round_trip_time_client2:
                            if self.my_package.time_recv_init:
                                print(self.my_package.time_recv_init)
                                self.client_data[client_connection].sendall(
                                    (
                                            f'{len(str(SERVER_INITIAL_SENT)):<{HEADERSIZE}}' +
                                            f'{SERVER_INITIAL_SENT}').encode())
                                self.my_package.time_send = datetime.now()
                                break
        except Exception as e:
            self.my_package = TimePackageServer()
            client_connection.close()
            print("send data Something goes wrong server ", e)

    # main function of server
    def start_master_server(self):
        try:
            # create socket
            server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server_socket.bind(('', self.port))
            server_socket.listen(10)
            print("Listening...")
            t1 = threading.Thread(target=self.handle_connections, args=(server_socket,))
            t2 = threading.Thread(target=self.send_data, args=())
            t1.start()
            t2.start()
        except socket.error as err:
            print("Create Socket server  Fail with error ", err)

    # function that calculate time of fligth using two ways ranging
    @staticmethod
    def calculate_time_of_fligth(round_trip_time_client1, round_trip_time_client2, round_trip_time_server2,
                                 round_trip_time_server1):
        rtt1_data = datetime.strptime(round_trip_time_client1, '%H:%M:%S.%f')
        if round_trip_time_client2 == "0:00:00":
            round_trip_time_client2_data = datetime.strptime(round_trip_time_client2, '%H:%M:%S')
        else:
            round_trip_time_client2_data = datetime.strptime(round_trip_time_client2, '%H:%M:%S.%f')
        return ((rtt1_data - round_trip_time_client2_data) + (round_trip_time_server2 - round_trip_time_server1)) / 4

    # function that return substring between two characters
    @staticmethod
    def find_between(s, first, last):
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""


if __name__ == "__main__":
    package = TimePackageServer()
    server = ServerTof(package)
    server.start_master_server()
