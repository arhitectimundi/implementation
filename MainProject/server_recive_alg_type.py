import socket
import sys
import subprocess
from PythonCristian.client_cristian_orange import ClientChristian
from PythonBerkeley.client_berkeley_orange import ClientBerkeley

HEADERSIZE = 10


class Server:

    def __init__(self, port=2991):
        self.port = port

    def start(self):
        try:
            serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            serversocket.bind(('', self.port))
            serversocket.listen(10)
            print("Listening...")
            self.handle_connection(serversocket)
        except socket.error as err:
            print("Create Socket server  Fail with error ", err)
            sys.exit(1)

    # method that saves the data about a connected client
    @staticmethod
    def recive_client_data(client_connection, client_adress):
        mslen = 0
        while True:
            full_msg = ''
            new_msg = True
            while len(full_msg) - HEADERSIZE != mslen:
                msg = client_connection.recv(16)
                if new_msg:
                    mslen = int(msg[:HEADERSIZE])
                    new_msg = False
                full_msg += msg.decode()
            print(client_adress[0])
            client_connection.close()
            return full_msg[HEADERSIZE:]

    # handle method for connection with server
    def handle_connection(self, server_connection):
        client_connection, addr = server_connection.accept()
        client_connection.setblocking(1)
        print("Connected to a client with ip " + str(addr[0]) + " and port " + str(addr[1]))
        command = self.recive_client_data(client_connection, addr)
        print(command)
        self.choose_client_algorithm(command, addr[0], client_connection, server_connection)

    @staticmethod
    def choose_client_algorithm(selected_algorithm, host, client_connection, server_connection):
        client_connection.close()
        server_connection.close()
        subprocess.Popen('python3 /home/pi/Desktop/Interface/client_tof1.py', shell=True).communicate()
        if selected_algorithm == str(0):
            return ClientChristian(host=host).start_client()
        else:
            return ClientBerkeley(host=host).client_connection()


if __name__ == "__main__":
    myServer = Server()
    myServer.start()
