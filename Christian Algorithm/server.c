/* Time server (concurrent mode).
  * Synchronization of clocks, following the centralized algorithm of Cristian. */

#include <string.h>

// Files needed to manage sockets, addresses, etc.

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define TAMBUF 100

// Commands and responses defined in the protocol.

char * commands [] =
{
"OPEN", // Login request.
"SYNC", // Start synchronization process.
"TERM" // Logout.
};

int command_compare(char *s1, char *s2);
// Compare two strings of exactly 4 characters, identifying the command.
int parser_commands (char *command);
// Identify the command in question.
void clear_buffer (char *buffer, int start, int stop);
void get (int s,struct sockaddr_in *info);
/*
// IPv4 AF_INET sockets:

struct sockaddr_in {
    short            sin_family;   // e.g. AF_INET, AF_INET6
    unsigned short   sin_port;     // e.g. htons(3490)
    struct in_addr   sin_addr;     // see struct in_addr, below
    char             sin_zero[8];  // zero this if you want to
};

struct in_addr {
    unsigned long s_addr;          // load with inet_pton()
};
*/

/***************************************************************************************************/

int main()
{
	int sock_listener,sock_dialog;		// connection and dialogue sockets.
	struct sockaddr_in dir_serv,dir_cli; 
	int port;
	socklen_t  length;

	length=sizeof(struct sockaddr);

	system("clear");

	write(1,"Cristian server> connect",25);

	printf("\n\nport (from %d): ",IPPORT_RESERVED+1);

	scanf("%d",&port);

	while (!(port > IPPORT_RESERVED))
	{
		printf("\n\n Port% d is reserved. Try again.",port);
		printf("\n\nport (from %d): ",IPPORT_RESERVED+1);
		scanf("%d",&port);
	}

	// Create the listening socket ...

	if ((sock_listener=socket(PF_INET,SOCK_STREAM,0)) < 0)
	{
		perror("\nSocket opening");
		exit(1);
	}

	dir_serv.sin_family=AF_INET;
	dir_serv.sin_port=htons(port);
	dir_serv.sin_addr.s_addr=htonl(INADDR_ANY);// Anyone can connect.

	if (bind(sock_listener,(struct sockaddr *)&dir_serv,sizeof(dir_serv)) < 0)
	{
		perror("\nBind");
		exit(1);
	}

	// We put the socket to listen ...

	if (listen(sock_listener,7) < 0)//The backlog argument defines the maximum length to which the queue of pending connections for sockfd may grow.
	{
		perror("\nListen");
		exit(1);
	}

	signal(SIGCHLD,SIG_IGN);	// avoid zombie children.

	printf ("\n \nAPPLICATION: Synchronization of clocks following the centralized Christian algorithm \n");
	printf ("Time server, waiting for requests ... \n");
	do
	{
		// Lock waiting for connection request.

		sock_dialog=accept(sock_listener,(struct sockaddr *)&dir_cli,&length);

		if (sock_dialog==-1)
		{
			// Special treatment for interruptions of dead children.

			if (errno==EINTR) continue;
			perror("\nAccept");
			exit(1);
		}

		else
			switch(fork())
			{
				case -1:

					perror("\nCreating a child");
					break;

				case 0:

					close(sock_listener);
					get(sock_dialog,&dir_cli);
					close(sock_dialog);
					exit(0);

				default:

					close(sock_dialog);
					break;
			}
	} while (1);

	exit(0);
}

/***************************************************************************************************/

int command_compare(char *s1, char *s2)
// Compare two strings of exactly 4 characters, identifying the command.

{
	int i;
	int result=1;

	for (i=0; i<4; i++)
		if (s1[i] != s2 [i])
			result=0;

	return (result);
}

int parser_commands (char *comando)
// Identify the command in question.

{
	if ((command_compare(comando, commands[0])) == 1) return (0); // OPEN
	if ((command_compare(comando, commands[1])) == 1) return (1); // SYNC
	if ((command_compare(comando, commands[2])) == 1) return (2); // TERM

	return (-1);
}

void clear_buffer (char *buffer, int start, int fin)
{
	int i;

	for (i=start; i<=fin; i++) buffer[i]=' ';
}

void get (int s,struct sockaddr_in *info)
{
	char *response[] =
	{
	"OKEY", // It can be accompanied by a parameter indicating the time (t (mt)) of the time server, or not carrying anything.
	"FINN" // May be accompanied by an explanatory string as to why the refusal occurred.
	};

	// auxiliary variables.

	int k,lon,lon2;
	char buf[TAMBUF];
	struct timeval serverhours;
	/*timeval struct have this structure 
	time_t         tv_sec      seconds
	suseconds_t    tv_usec     microseconds*/
	struct tm *tmp;
	/*int tm_sec;	(seconds after the minute)
	int tm_min;		(minutes after the hour)
	int tm_hour;	(hours since midnight)
	int tm_mday;	(day of the month)
	int tm_mon;		(months since January)
	int tm_year;	(years since 1900)
	int tm_wday;	(days since Sunday)
	int tm_yday;	(days since January 1)
	int tm_isdst;	(Daylight Saving Time flag)
	*/

	// Wait for the OPEN command to be sent.

	k=read(s,buf,TAMBUF);

	if (k<0)
	{
		perror("\nOPEN Command Reception");
		return;
	}

	buf[k]='\0';

	if ((command_compare(commands[0],buf)) == 0)
	{
		perror("\nUnknown command");
		sprintf(buf,"%s: OPEN command expected",response[1]);
		write(s,buf,strlen(buf));
	}

	// All right. We return OKEY and wait for them to send us a command, which from this moment
	// can be: SYNC or TERM

	sprintf(buf,"%s: Open session",response[0]);
	write(s,buf,strlen(buf));
	printf("\nIP =%d Connected\n",ntohl(info->sin_addr.s_addr));

	// We enter the command reception loop.

	do
	{
		k=read(s,buf,TAMBUF);

		if (k<0)
		{
			perror("\nReception a command");
			return;
		}

		buf[k]='\0';
		switch(parser_commands(buf))
		{
			case 1:	// SYNC

				/* calculate the server time */

				gettimeofday(&serverhours,NULL);

				// the server sends you as response:
				// OK +
				// seconds (the value starts in &buf[10]) +
				// microseconds (the value starts in &buf[35])

				sprintf(buf,"%s: secounds:%ld",response[0],serverhours.tv_sec);
				lon=strlen(buf);
				clear_buffer(buf,lon+1,29);
				sprintf(&buf[30],"usec:%ld",serverhours.tv_usec);
				lon2=strlen(&buf[30]);
				write(s,buf,30+lon2);

				tmp=(struct tm*)localtime(&(serverhours.tv_sec));
				printf ("\nIP =%d Synchronizing ... \n", ntohl (info-> sin_addr.s_addr));
				printf ("t(mt): %d h. %d m. %d s. %ld us. \n", tmp-> tm_hour, tmp->tm_min, tmp->tm_sec, serverhours.tv_usec);
				break;

			case 2:	// TERM

				sprintf (buf, "%s: Logging out ...", response [0]);
				write(s,buf,strlen(buf));
				close(s);
				printf("\nIP=%d       Disconected\n",ntohl(info->sin_addr.s_addr));
				return;

			case 0:	// OPEN

				sprintf(buf, "%s: Client protocol error", response [1]);
				write(s,buf,strlen(buf));
				close(s);
				return;

			default:

				sprintf (buf, "%s: Command unknown", response [1]);
				write(s,buf,strlen(buf));
				close(s);
				return;
		}
	} while (1);
}

