// Client for synchronization of clocks using the Cristian algorithm, directly using the socket interface.

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>
#include <netdb.h>  			// to use gethostbyname
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/timeb.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#define TAMBUF 100

char nom_serv [80]; 							// name of the server machine
char * commands [] = {"OPEN", "SYNC", "TERM"};  // communication commands with the server
char * answers [] = {"OKEY", "FINN"}; 			// server responses

struct timeval Dmin;

int parser_responses(char * answer);
// Identify the command in question
int calculate_absolute_deviations (long * s, long * us);
// Return 1 if the local clock was advanced.
// -1 if the local clock was delayed.
// 0 if the local clock was set.
int smallerthenDmin (struct timeval * newD);
// Returns 1 if the time D that is passed as an argument is less than what was at least so far.
// 0 e. or. C.
void synchronize(int s);
void finish(int s);

int main()
{
	int sock, k, port;
  	struct sockaddr_in server;
  	struct hostent *hp, *gethostbyname();
	// struct hostent {
        //    char  *h_name;            /* official name of host */
        //    char **h_aliases;         /* alias list */
        //    int    h_addrtype;        /* host address type */
        //    int    h_length;          /* length of address */
        //   char **h_addr_list;       /* list of addresses */
        //  }
        //  #define h_addr h_addr_list[0] /* for backward compatibility 

  	char buf[TAMBUF];
	char option[50];

	system("clear");

	printf ("APPLICATION: Synchronization of clocks following the centralized algorithm of CHristian\n");


	// ask server name.
	printf ("Enter the name of the local network server \n (Example: acpt00.gi.ehu.es or localhost):+");
	scanf("%s", nom_serv);

	printf("\n\nport (starting with %d): ",IPPORT_RESERVED+1);
	scanf("%d", &port);

	while (port <= IPPORT_RESERVED)
	{
		printf("\n\nPort %d is reserved. Introduce other one.\n",port);
		printf("\n\nport (starting with %d): ",IPPORT_RESERVED+1);
		scanf("%d", &port);
	}

    // Open the socket to establish the connection.

  	sock = socket(PF_INET, SOCK_STREAM, 0);
  	if (sock <0)
	{
		perror("Cannot create socket");
	  	exit(1);
  	}

  	server.sin_family = AF_INET;
  	server.sin_port = htons(port);

  	// Extract the server information from the DNS.

  	hp = gethostbyname(nom_serv);
  	if (hp == NULL)
  	{
		perror("gethostbyname");
	  	close(sock);
	  	exit(1);
  	}

  	memcpy(&server.sin_addr, hp->h_addr, hp->h_length);

  	if (connect(sock,(struct sockaddr*)&server, sizeof server) < 0)
	{
		perror("connect");
	  	close(sock);
	  	exit(1);
  	}

// Start the application protocol with the sending of OPEN command.

 printf ("\n \nConnecting to port %d of the machine,%s ... \n", port, nom_serv);
 sprintf (buf, "%s", commands[0]);
 write (sock, buf, strlen(buf));

 // if we are here, we must receive the confirmation OKEY.

 if ((k == read (sock, buf, TAMBUF)) <0)
 {
		perror ("read");
		close (sock);
		exit (1);
 }

 switch (parser_responses(buf))
 {
 case 0: // correct login
printf ("\n Logon established ... \n");
break;

 case 1: // login failed.
printf ("\n Session failed ... \n");
close (sock);
exit (1);

 default: // we don't know what happened ...
printf ("\n Protocol error ... Abandonment. \n");
close (sock);
exit (1);
 }

// Initialize Dmin with a large enough value.

Dmin.tv_sec = 1000; //for example. (1000 s.)
Dmin.tv_usec = 1000000;

  	while(1)
  	{
		printf("\n--------------------------------------");
		printf("\n             Choose the option you want:");
	  	printf("\n              1.- synchronize the clock");
	  	printf("\n             2.- Exit");
		printf("\n--------------------------------------\n\n");
	  	scanf("%s",option);

	  	switch(option[0])
	     	{
	       		case '1':	synchronize(sock); break;
	       		case '2':	finish(sock); break;
               		default:	printf("\nunknown option. Try again.\n"); break;
	     	}
  	}
}

int compare_responses(char *s1,char *s2)
// Compare two strings of exactly 4 characters
{
	int i;
	int result = 1;
	for (i=0; i<4; i++)
		if(s1[i] != s2[i]) result = 0;
	return(result);
}

int parser_responses(char *reply)
// Identify the command in question
{
	if ((compare_responses(reply,answers[0])) == 1) 
return(0); //OKEY
	if ((compare_responses(reply,answers[1])) == 1) 
return(1); //FINN
	return(-1);
}

int calculate_absolute_deviations(long *sec, long *usec)
	// Return 1 if the local clock was advanced.
	// -1 if the local clock was delayed.
	// 0 if the local clock was set.
{
	if (*sec > 0)
	{
		printf("advanced");
		if (*usec < 0)
		{
			*usec=(*usec) + 1000000;//1s = 10^6 us
			*sec--;
		}
		return(1);
	}
	else if (*sec == 0)
	{
		if (*usec > 0)
		{
			printf("advanced.");
			return(1);
		}
		else if (*usec == 0)
		{
			printf("adjusted.");
			return(0);
		}
		else
		{
			printf("delayed.");
			*usec=(*usec) * (-1);
			return(-1);
		}
	}
	else
	{
		*sec=(*sec) * (-1);
		printf("delayed.");
		if (*usec > 0)
		{
			*usec=(*usec) * (-1) + 1000000;
			*sec=(*sec) - 1;
		}
		else if (*usec < 0) *usec=(*usec) * (-1);
		return(-1);
	}
}

int smallerthenDmin(struct timeval *newTime)
	// Returns 1 if the time newTime that is passed as an argument is less than what was at least so far.
	// 0 e. or. C.
{
	if (newTime->tv_sec < Dmin.tv_sec) return(1);
	else if (newTime->tv_sec > Dmin.tv_sec) return(0);
	else
	{
		if (newTime->tv_usec < Dmin.tv_usec) return(1);
		else return(0);
	}
}

void synchronize(int s)
{
  	char buf[TAMBUF];
	int k, advanced;

	struct timeval t0, t1, D, deviation, absolutedeviation, tmt, t, adjustment;
	struct tm *tmp, *tmp2, *tmp3;

	system("clear");

	// We write the current time just before the time request to the server.

	gettimeofday(&t0, NULL);

	// We send the SYNC command to start synchronization.

	sprintf (buf, "%s", commands[1]);
	if (write(s, buf, strlen(buf))<strlen(buf))
	{
	perror ("Error sending SYNC command. Abandonment ..");
	close(s);
	exit(0);
	}

	// We receive the time provided by the server.

	if ((k = read (s, buf, TAMBUF)) <0)
	{
	perror ("Reading the time received by the server.");
	exit (1);
	}

	// Note the current time, after receiving the server time.
	gettimeofday (&t1, NULL);
	// Calculation of D. D = t1-t0

	D.tv_sec = t1.tv_sec-t0.tv_sec;
	D.tv_usec = t1.tv_usec-t0.tv_usec;

	// I have subtracted seconds on the one hand and microseconds on the other hand. The difference (t1-t0) may not be real.
	// t1> t0

	if (D.tv_usec <0)
	{
	D.tv_usec = D.tv_usec + 1000000;
	D.tv_sec--;
	}

	printf ("Attempt. D equal to,%ld sec. and %ld usec. \n", D.tv_sec, D.tv_usec);

// Manipulate the server time received.

	tmt.tv_sec=atol(&buf[10]);			// seconds and microseconds of t (mt).
	tmt.tv_usec=atol(&buf[35]);
	tmp=(struct tm*)localtime(&(tmt.tv_sec));

	t.tv_sec=t1.tv_sec-(D.tv_sec/2);
	t.tv_usec=t1.tv_usec-(D.tv_usec/2);

	// I have subtracted seconds on the one hand and microseconds on the other hand. The difference may not be real.

	if (t.tv_usec < 0)
	{
		t.tv_usec=t.tv_usec + 1000000;
		t.tv_sec--;
	}

	tmp2=(struct tm*)localtime(&(t.tv_sec));

	printf("\n t: %d hrs. %d mns. %d sec. %ld usec.",tmp2->tm_hour,tmp2->tm_min,tmp2->tm_sec,t.tv_usec);
	printf("\n           t(mt): %d hrs. %d mns. %d sec. %ld usec. (Server Time)\n",tmp->tm_hour,tmp->tm_min,tmp->tm_sec,tmt.tv_usec);
	// Calculate the time difference between our clock and the server's clock, and adjust it.

	// The current deviation of the local clock will be given by: deviation = t-t(mt)

	printf("\n You will have the clock ");

	absolutedeviation.tv_sec=t.tv_sec-tmt.tv_sec;
	absolutedeviation.tv_usec=t.tv_usec-tmt.tv_usec;

	// I have subtracted seconds on the one hand and microseconds on the other hand.

	advanced=calculate_absolute_deviations(&absolutedeviation.tv_sec,&absolutedeviation.tv_usec);

	// advanced takes the value: 1 if the local clock is advanced.
	// -1 if the local clock is delayed.
	// 0 if the local clock is set.


	tmp3=(struct tm*)localtime(&(absolutedeviation.tv_sec));
	printf("\ndeviation |t- t(mt)|: %d hrs. %d mns. %d sec. %ld usec.\n\n",tmp3->tm_hour,tmp3->tm_min,tmp3->tm_sec,absolutedeviation.tv_usec);

	printf ("The value of D obtained in this attempt,\n");		

	if (smallerthenDmin(&D) == 0)
	{
		printf ("It is NOT LESS than the minimum obtained so far. \n");
printf ("Minimum D:%ld sec. and %ld usec. \n\n\nYou choose NOT to adjust. There are no guarantees of better accuracy. \n \n", Dmin.tv_sec, Dmin.tv_usec);
	}
	else
	{
		printf ("IF it is LESS than the minimum obtained so far. \n");
		Dmin.tv_sec=D.tv_sec;
		Dmin.tv_usec=D.tv_usec;
		printf ("New minimum D:%ld sec. and %ld usec. \n \n \n", Dmin.tv_sec, Dmin.tv_usec);

		gettimeofday(&adjustment,NULL);

		if (advanced==1) 		// subtract difference
		{
			adjustment.tv_usec=adjustment.tv_usec-absolutedeviation.tv_usec;

			if (adjustment.tv_usec < 0)
			{
				adjustment.tv_usec=adjustment.tv_usec + 1000000;
				adjustment.tv_sec--;
			}
			adjustment.tv_sec=adjustment.tv_sec-absolutedeviation.tv_sec;

			if ((settimeofday(&adjustment,NULL)) <0) perror("Adjusting clock ...");
			else
			{
				tmp3=(struct tm*)localtime(&(adjustment.tv_sec));
				printf("              Clock adjust synchronization : %d hrs. %d mns. %d sec. %ld usec.\n\n",tmp3->tm_hour,tmp3->tm_min,tmp3->tm_sec,adjustment.tv_usec);
			}
		}
		else if (advanced==-1)	//sumar difference
		{
			adjustment.tv_usec=adjustment.tv_usec+absolutedeviation.tv_usec;

			if (adjustment.tv_usec >= 1000000)
			{
				adjustment.tv_usec=adjustment.tv_usec - 1000000;
				adjustment.tv_sec++;
			}
			adjustment.tv_sec=adjustment.tv_sec+absolutedeviation.tv_sec;

			if ((settimeofday(&adjustment,NULL)) <0) perror("Setting clock ...");
			else
			{
				tmp3=(struct tm*)localtime(&(adjustment.tv_sec));
				printf("               Clock synchronization : %d hrs. %d mns. %d sec. %ld usec.\n\n",tmp3->tm_hour,tmp3->tm_min,tmp3->tm_sec,adjustment.tv_usec);
			}
		}
		else				//it is ok
		{
			tmp3=(struct tm*)localtime(&(adjustment.tv_sec));
			printf("               Clock synchronization: %d hrs. %d mns. %d sec. %ld usec.\n\n",tmp3->tm_hour,tmp3->tm_min,tmp3->tm_sec,adjustment.tv_usec);
		}
	}
}

void finish(int s)
{
	char buf[TAMBUF];
	int k;

	// We send the TERM command

	sprintf(buf,"%s",commands[2]);

	if (write(s, buf, strlen(buf)) < strlen(buf))
	{
		perror("Error sending TERM command, Abandonment ..");
		close(s);
		exit(0);
	}


	if ((k = read(s, buf, 4)) < 0)
	{
		perror("Reading response to the TERM command");
		exit(1);
	}

	// We await the reception of  OKEY.

	switch(parser_responses(buf))
	{
		case 0:	// Correct, we close the connection.
			close(s);
			exit(0);
		case 1:	//Something's wrong.
		
		
		
			printf("Rejected TERM \n");
			close(s);
			return;
			close(s);
			exit(1);
	}
}
