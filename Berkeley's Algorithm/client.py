import socket
import datetime
import time
import threading
from dateutil import parser

class Client:
    def __init__(self,port = 2997, host = 'localhost'):
        self.host = host
        self.port = port

#function to send time to the server
    def send_time(self,client_socket):
        while True:
            client_socket.send(str(datetime.datetime.now()).encode())
            print("Client_time" , datetime.datetime.now())
            time.sleep(5)

#function for reciving server time :
    def recive_time(self,client_socket):
        while True:
            sync_time = parser.parse(client_socket.recv(1024).decode())
            print("Synchronized time ",sync_time)
#main function of class
    def client_connection(self):
        try:
            #create client socket
            clientSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            #connect to server
            clientSocket.connect((self.host,self.port))
            threading.Thread(target=self.send_time,args = (clientSocket, )).start()
            threading.Thread(target=self.recive_time,args=(clientSocket, )).start()
        except Exception as e:
            print("Something goes wrong : ",e)


if __name__ == "__main__":
    client_server = Client()
    client_server.client_connection()