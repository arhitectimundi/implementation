import socket
import threading
import datetime
import sys
import time
from dateutil import parser


class Server:
#constructor of Server
    def __init__(self,port=2997,client_data = {}):
        self.client_data = client_data
        self.port = port


#recive the data from the clients and save in a dictionary where key = client_adress,
# data =  client_time,difference between client and master time,and client connection
    def recive_client_data(self,client_connection,client_adress):
        while True:
            client_time = parser.parse(client_connection.recv(1024).decode())
            master_client_time_diff = datetime.datetime.now() - client_time
            self.client_data[client_adress] = {
                "client time" : client_time,
                "master client diff " : master_client_time_diff,
                "client connection " : client_connection
            }
            time.sleep(5)
            

#function to handle connection of server to master
    def handle_connections(self, master_server):
        while True:
            client_connection, addr = master_server.accept()
            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            threading.Thread(target=self.recive_client_data,args = (client_connection,addr, )).start()


#calculating the average time difference between clients and server
    def calculate_average_clock_diff(self):
        data_list = list(client_connection["master client diff "]
                                for _,client_connection in self.client_data.items())
        average_clock_diff = sum(data_list,datetime.timedelta(0,0))/len(self.client_data)
        print("Time diff is ",average_clock_diff)
        return average_clock_diff
    
#syncronize the client clocks
    def syncronize_clients(self):
        while True:
            if len(self.client_data) > 0:
                average_clock_diff = self.calculate_average_clock_diff()
                for _,client_connection in self.client_data.items():
                    try:
                        sync_time = datetime.datetime.now() + average_clock_diff
                        client_connection["client connection "].send(str(sync_time).encode())
                    except  Exception as e :
                        print("Something goes wrong ", e)
            else:
                print("No data from client")
                print("\n\n")
            time.sleep(5)
#main function of a server
    def start_master_server(self,port = 2997):
        try:
            serversocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            serversocket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
            serversocket.bind(('',self.port))
            serversocket.listen(10) 
            print("Listening...")
            threading.Thread(target = self.handle_connections, args = (serversocket, )).start()
            threading.Thread(target= self.syncronize_clients, args =()).start()
        except socket.error as err:
            print ("Create Socket server  Fail with error ", err)
            sys.exit(1)

    
    
if __name__ == "__main__":
    my_server= Server()
    my_server.start_master_server()
    

