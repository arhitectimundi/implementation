import socket
import time
import sys
import os
from datetime import datetime, timedelta
from threading import Thread
import traceback
from dateutil import parser

HEADERSIZE = 10
BYTE = 8
#dictionar ce salveaza TOF pentru dispozitivele conectate
#RASP - raspberry pi iar ORANGE - orange pi
# SAME - indica ca serverul si dispozitivul sunt in acelasi WLAN
# NSAME - serverul si dispozitivul in WLAN-uri diferite
# Wx - indica wlanul
TOF_DICT = {"TOF_SAME_RASP_W1": "0:00:00.005496",
            "TOF_SAME_ORANGE_W1": "0:00:00.006091",
            "TOF_NSAME_RASP_W2": "0:00:00.007116"}


class ServerChristian:

    def __init__(self, connected_list={}, port=2997):
        self.connected_list = connected_list
        self.port = port

    def start_server(self):
        try:
            # se creaza socketul pentru a acepta noi clienti
            server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # serverul accepta orice interfata
            server_socket.bind(('', self.port))
            # se vor accepta max. 10 conexiuni, ele fiind indeajuns pentru aplicatia data
            server_socket.listen(10)
            print("Listening...")
            # se pornesc 2 fire de executie
            # Unul pentru a mentine conexiunea cu clientul
            # Celălalt pentru a trimite raspuns la cererea clientului
            handle_connection_thread = Thread(target=self.handle_connection, args=(server_socket,))
            handle_connection_thread.start()
            send_clock_thread = Thread(target=self.send_clock_to_client, args=())
            send_clock_thread.start()
        except socket.error as err:
            print("Create Socket server  Fail with error ", err)
            sys.exit(1)

    # functie ce mentine conexiunea cu clientii
    def handle_connection(self, server_connection):
        while True:
            # se realizează conexiunea cu clientul
            client_connection, addr = server_connection.accept()

            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            try:
                # se recepționează datele de la client
                recive_thread = Thread(target=self.recive_client_data, args=(client_connection, addr,))
                recive_thread.start()
            except Exception as e:
                print("Thread did not start.")
                traceback.print_exc()

    def recive_client_data(self, client_connection, client_adress):
        mslen = 0
        # se receptionează semnalul trimis de client
        try:
            while True:
                # se salvează conexiunea cu clientul într-un dictionar
                self.connected_list[client_adress] = client_connection
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != mslen:
                    # se receptioneaza datele de la server
                    msg = client_connection.recv(BYTE)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                recived_message = parser.parse(full_msg[HEADERSIZE:])
                # se specifica TOF la conexiunea cu unul dintre device-uri
                if socket.gethostbyaddr(client_adress[0])[0] == "orangepizero.zte.com.cn":
                    time_of_fligth = datetime.strptime(TOF_DICT["TOF_SAME_ORANGE_W1"], '%H:%M:%S.%f')
                else:
                    time_of_fligth = datetime.strptime(TOF_DICT["TOF_SAME_RASP_W1"], '%H:%M:%S.%f')
                actual_time = datetime.now()
                # se calculeaza diferenta dintre timppul serverului si al clientului
                # calulul e efectuat luand in vedere si timpul de zbor al raspunsului
                difference = abs(actual_time - (recived_message - timedelta(seconds=time_of_fligth.second)))
                concatenated_data = str(recived_message) + "," + str(actual_time) + "," + str(difference)
                Thread(target=self.write_to_file, args=(concatenated_data, str(client_adress[0]))).start()

        except (ConnectionResetError, ConnectionAbortedError, ValueError, KeyError, TimeoutError):
            # În cazul unei erori clientul este sters
            # conexiunea cu acesta fiind închisă
            print("am ajuns in handle")
            self.connected_list.pop(client_adress)
            client_connection.close()
            if not self.connected_list:
                os._exit(0)

    # scriu datele necesare in fisier
    @staticmethod
    def write_to_file(data, file_name):
        file = open("C:\\Users\\iongu\\Desktop\\Interface\\SavedData\\" + file_name + ".txt", "a+")
        file.write(data)
        file.write("\n")
        time.sleep(1)
        file.close()

    def send_clock_to_client(self):
        # Se trimite răspunsul la cererea clientului
        while True:
            # cât timp există clienți conectați
            # se formează răspunsul
            if len(self.connected_list) > 0:
                for client_connection in self.connected_list:
                    try:
                        # pentru fiecare client se transmite
                        # ora actuală a serverului
                        self.connected_list[client_connection].send(
                            (f'{len(str(datetime.now())):<{HEADERSIZE}}' + f'{datetime.now()}').encode())
                    except Exception as e:
                        print("Something goes wrong ", e)

            # serverul va trimite date spre client la fiecare 1 secunde
            time.sleep(1)


if __name__ == "__main__":
    ServerChristian().start_server()
