import subprocess
import shlex
import socket
import threading
import time
from datetime import datetime, timedelta
from dateutil import parser
from timeit import default_timer
# Import Raspberry Pi GPIO library
import RPi.GPIO as GPIO

GPIO_NUM = 8
HEADERSIZE = 10
CLOCK_REALTIME = 0
MAX_NUM_OF_INTERUPTS = 4


class ClientChristian:
    def __init__(self, port=2997, host='192.168.1.7'):
        self.port = port
        self.host = host
        self.calculated_client_time = 0
        self.final_data = list()
        self.num_of_interrupts = 0

    def button_callback(self, _):
        if not len(self.final_data):
            self.final_data.clear()
        if self.calculated_client_time:
            self.final_data.append(self.calculated_client_time)
        self.num_of_interrupts += 1

    def button_settings(self):
        # se ignora atentionarile ulterioare
        GPIO.setwarnings(False)
        # Se foloseste numerotarea fizica a pinilor
        GPIO.setmode(GPIO.BOARD)
        # pinul GPIO este setat ca pin de intrare cu valoarea initiala pull_down
        GPIO.setup(GPIO_NUM, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # functia de callback se va apela pe frontul pozitiv
        GPIO.add_event_detect(GPIO_NUM, GPIO.RISING, callback=self.button_callback, bouncetime=200)

    def recive_data(self, client_server):
        self.button_settings()
        subprocess.call(shlex.split("sudo timedatectl set-ntp false"))  # May be necessary
        mslen = 0
        # dezactivam utilizarea seasului utilizand NTP
        try:
            while True:
                full_msg = ''
                new_msg = True
                # defaul_timer indica un timer cu o precizie de microsecunde
                time_of_request = default_timer()
                while len(full_msg) - HEADERSIZE != mslen:
                    # se receptioneaza datele de la server
                    msg = client_server.recv(16)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                # print("full_msg " + str(full_msg))
                server_time = parser.parse(full_msg[HEADERSIZE:])
                # calculate time of reciving response
                time_of_response = default_timer()
                round_trip_time = time_of_response - time_of_request
                # calculate normal clock time
                self.calculated_client_time = server_time + timedelta(seconds=round_trip_time / 2)
                subprocess.call(shlex.split("sudo date -s '%s'" % self.calculated_client_time))
                # error
                error = abs(server_time - self.calculated_client_time)

                print("Server_Time ", server_time)
                # print("Round_Trip_Time", round_trip_time)
                # print("Real Client time ", actual_client_time)
                # print("Calculated_client_time ", self.calculated_client_time)
                print("Error betwween server and client is ", error)
                print("\n\n")
        except (ConnectionResetError, ConnectionRefusedError):
            print("Conexiune refuzată/resetată! ")
            client_server.close()

    def send_data(self, socket_client):

        # self.final_data.append("6")
        # daca a fost apasat butonul
        # se poate adauga while TRUE
        while True:
            if len(self.final_data):
                try:
                    print("Am ajuns sa trimit data")
                    if self.num_of_interrupts == MAX_NUM_OF_INTERUPTS:
                        print("Ai apasat de 4 ori")
                        socket_client.close()
                    data = self.final_data.pop()
                    socket_client.sendall((f'{len(str(data)):<{HEADERSIZE}}' + f'{data}').encode())
                    # socket_client.close()
                except Exception as err:
                    print('err')
            time.sleep(1)

    def start_client(self):
        try:
            # se creeaza un socket pentru conexiunea cu serverul
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # se conectează la interfața serverului
            clientSocket.connect((self.host, self.port))
            # informatiile de la server sunt primite, prelucrate
            # dupa care se retransmite cererea
            send_thread = threading.Thread(target=self.send_data, args=(clientSocket,))
            send_thread.start()
            recive_thread = threading.Thread(target=self.recive_data, args=(clientSocket,))
            recive_thread.start()
        except socket.error as err:
            print("Create Socket client fail with error ", err)


if __name__ == "__main__":
    ClientChristian().start_client()

