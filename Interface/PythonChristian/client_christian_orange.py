import subprocess
import os
import shlex
import socket
import threading
import time
from datetime import datetime, timedelta
from dateutil import parser
from timeit import default_timer
# Import Orange Pi GPIO library
from pyA20.gpio import gpio
from pyA20.gpio import port

GPIO_NUM = port.PA6
HEADERSIZE = 10
CLOCK_REALTIME = 0
MAX_NUM_OF_INTERUPTS = 5


class ClientChristian:
    def __init__(self, port=2997, host='192.168.1.7'):
        self.port = port
        self.host = host
        self.calculated_client_time = 0
        self.final_data = list()
        self.num_of_interrupts = 0

    def button_callback(self):
        if not len(self.final_data):
            self.final_data.clear()
        if self.calculated_client_time:
            self.final_data.append(self.calculated_client_time)
        self.num_of_interrupts += 1

    def button_settings(self):
        # se ignora atentionarile ulterioare
        initial_button_state = True
        gpio.init()
        # Se foloseste numerotarea fizica a pinilor
        gpio.setcfg(GPIO_NUM, gpio.INPUT)
        # pinul GPIO este setat ca pin de intrare cu valoarea initiala pull_down
        gpio.pullup(GPIO_NUM, gpio.PULLUP)
        # se apeleaza
        while True:
            current_button_state = gpio.input(GPIO_NUM)
            if current_button_state == initial_button_state:
                self.button_callback()
            time.sleep(0.200)

    def recive_data(self, client_server):
        threading.Thread(target=self.button_settings).start()
        subprocess.call(shlex.split("sudo timedatectl set-ntp false"))  # May be necessary
        mslen = 0
        # dezactivam utilizarea seasului utilizand NTP
        try:
            while True:
                full_msg = ''
                new_msg = True
                # defaul_timer indica un timer cu o precizie de microsecunde
                time_of_request = default_timer()
                while len(full_msg) - HEADERSIZE != mslen:
                    # se receptioneaza datele de la server
                    msg = client_server.recv(16)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                # print("full_msg " + str(full_msg))
                server_time = parser.parse(full_msg[HEADERSIZE:])
                # calculate time of reciving response
                time_of_response = default_timer()
                round_trip_time = time_of_response - time_of_request
                # calculate normal clock time
                self.calculated_client_time = server_time + timedelta(seconds=round_trip_time / 2)
                # setam data calculata ca fiind cea a deviceului
                subprocess.call(shlex.split("sudo date -s '%s'" % self.calculated_client_time))
                # erroarea actuala pe device
                error = abs(server_time - self.calculated_client_time)

                print("Server_Time ", server_time)
                print("Error betwween server and client is ", error)
                print("\n\n")
        except (ConnectionResetError, ConnectionRefusedError):
            print("Conexiune refuzata/resetata! ")
            client_server.close()
            os._exit(1)

    def send_data(self, socket_client):

        # self.final_data.append("6")
        # daca a fost apasat butonul
        # se poate adauga while TRUE
        while True:
            if len(self.final_data):
                try:
                    print("Am ajuns sa trimit data")
                    data = self.final_data.pop()
                    socket_client.sendall((f'{len(str(data)):<{HEADERSIZE}}' + f'{data}').encode())
                    if self.num_of_interrupts == MAX_NUM_OF_INTERUPTS:
                        print("Ai apasat de 4 ori")
                        # time.sleep(1)
                        os._exit(1)
                    # socket_client.close()
                except Exception as err:
                    print('err')
            time.sleep(1)

    def start_client(self):
        try:
            # se creeaza un socket pentru conexiunea cu serverul
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # se conectează la interfața serverului
            clientSocket.connect((self.host, self.port))
            # informatiile de la server sunt primite, prelucrate
            # dupa care se retransmite cererea
            send_thread = threading.Thread(target=self.send_data, args=(clientSocket,))
            send_thread.start()
            recive_thread = threading.Thread(target=self.recive_data, args=(clientSocket,))
            recive_thread.start()
        except socket.error as err:
            print("Create Socket client fail with error ", err)


if __name__ == "__main__":
    ClientChristian().start_client()
