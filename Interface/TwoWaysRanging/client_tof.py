import socket
import datetime
import time
from dataclasses import dataclass
import threading
import os

HEADERSIZE = 10
BYTE = 8
CLIENT_INITIAL_SENT = 'Csi'
CLIENT_FINAL_SENT = 'Csf'
SERVER_INITIAL_SENT = 'Ssi'


@dataclass
class TimePackage:
    time_send_init: datetime = 0
    time_recv_init: datetime = 0
    time_send_final: datetime = 0


class Client:
    def __init__(self, my_package, port=2997, host='192.168.1.7'):
        self.host = host
        self.port = port
        self.my_package = my_package

    # function to send data to server
    def send_time(self, client_socket):
        flag = True
        try:
            while True:
                # save time when first data is sent
                if self.my_package.time_send_init == 0:
                    self.my_package.time_send_init = datetime.datetime.now()
                    client_socket.sendall((f'{len(str(CLIENT_INITIAL_SENT)):<{HEADERSIZE}}' +
                                           f'{CLIENT_INITIAL_SENT}').encode())
                    # if package from server is
                elif self.my_package.time_recv_init and flag:
                    self.my_package.time_send_final = datetime.datetime.now()
                    client_socket.sendall(
                        (f'{len(str(CLIENT_FINAL_SENT)):<{HEADERSIZE}}' + f'{CLIENT_FINAL_SENT}').encode())
                    flag = False
                    # dupa ce datele au fost transmise se calculeaza duratele de timp intre momentul trimeterii/sosirii.
                # ambele date se trimit concatenate
                elif self.my_package.time_recv_init and self.my_package.time_send_final:
                    round_trip_time1 = abs(self.my_package.time_recv_init - self.my_package.time_send_init)
                    round_trip_time2 = abs(self.my_package.time_send_final - self.my_package.time_recv_init)
                    data_send = 'i1' + str(round_trip_time1) + 'f1' + 'i2' + str(round_trip_time2) + 'f2'
                    client_socket.sendall((f'{len(str(data_send)):<{HEADERSIZE}}' + f'{data_send}').encode())
                    self.my_package = 0
                    time.sleep(1)
                    os._exit(0)

        except AttributeError as e:
            print(e)

    # functie pentru receptionarea datelor :
    def recive_time(self, client_socket):
        mslen = 0
        try:
            while True:
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != mslen:
                    # se receptioneaza datele de la server
                    # toate datele transmise in aplicatie se transmit in 2 pachete
                    # de accea nu am impartit TOF(time of fligth) la 2!
                    msg = client_socket.recv(16)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False
                    full_msg += msg.decode()
                recive_data = full_msg[HEADERSIZE:]
                if str(SERVER_INITIAL_SENT) in recive_data:
                    self.my_package.time_recv_init = datetime.datetime.now()
        except (ConnectionResetError, ConnectionRefusedError):
            print("Conexiune refuzată/resetată! ")
            client_socket.close()

    # functia principala a aplicatiei
    def client_connection(self):
        try:
            # creem socketul
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # se conecteaza la server
            clientSocket.connect((self.host, self.port))
            # se pornesc fire de executie pentru transmiterea/receptionarea datelor
            t1 = threading.Thread(target=self.send_time, args=(clientSocket,))
            t1.start()
            t2 = threading.Thread(target=self.recive_time, args=(clientSocket,))
            t2.start()
        except Exception as e:
            print("Something goes wrong client : ", e)


if __name__ == "__main__":
    my_pack = TimePackage()
    client_server = Client(my_pack)
    client_server.client_connection()
