from dataclasses import dataclass
import socket
import threading
from datetime import  datetime
import time
import os

#variabilele globale
HEADERSIZE = 10
BYTE = 8
SERVER_INITIAL_SENT = 'Ssi'
CLIENT_INITIAL_SENT = 'Csi'
CLIENT_FINAL_SENT = 'Csf'

# clasa respectiva are functia unei structuri 
# ce salveaza toate datele necesare calcularii TOF
@dataclass
class TimePackage:
    time_recv_init: datetime = 0
    time_send: datetime = 0
    time_recv_final: datetime = 0
    
    round_trip_time_client1: datetime = 0
    round_trip_time_client2: datetime = 0
    round_trip_time_server1: datetime = 0
    round_trip_time_server2: datetime = 0


class Server:
    # constructor of Server
    def __init__(self, my_package, port=2997, client_data=None):
        if client_data is None:
            client_data = {}
        self.client_data = client_data
        self.port = port
        self.my_package = my_package

    # se receptioneaza datele de la client intr-o structura unde cheia = client_adress,
    # valoarea = conexiunea cu clientul
    def recive_client_data(self, client_connection, client_adress):
        mslen = 0
        try:
            while True:
                self.client_data[client_adress] = client_connection
                full_msg = ''
                new_msg = True
                while len(full_msg) - HEADERSIZE != mslen:
                    # se receptioneaza datele de la server
                    msg = client_connection.recv(BYTE)
                    if new_msg:
                        mslen = int(msg[:HEADERSIZE])
                        new_msg = False

                    full_msg += msg.decode()
                recive_data = full_msg[HEADERSIZE:]
                # se salveaza timpul cand se receptioneaza primul pachet de la client
                if recive_data == str(CLIENT_INITIAL_SENT):
                    self.my_package.time_recv_init = datetime.now()
                # se salveaza timpul cand se recep. al 2 pachet de la client
                elif recive_data == str(CLIENT_FINAL_SENT):
                    self.my_package.time_recv_final = datetime.now()
                else:
                    # se receptioneaza round_trip_time calculate de client
                    if self.find_between(recive_data, "i1", "f1"):
                        self.my_package.round_trip_time_client1 = self.find_between(recive_data, "i1", "f1")
                    if self.find_between(recive_data, "i2", "f2"):
                        self.my_package.round_trip_time_client2 = self.find_between(recive_data, "i2", "f2")
                    # daca s-au obtinut toate datele se calculeaza TIME_OF_FLIGTH
                    if self.my_package.time_recv_final != 0:
                        self.my_package.round_trip_time_server2 = abs(self.my_package.time_recv_final - self.my_package.time_send)
                        self.my_package.round_trip_time_server1 = abs(self.my_package.time_send - self.my_package.time_recv_init)
                        TOF = self.calculate_time_of_fligth(self.my_package.round_trip_time_client1, self.my_package.round_trip_time_client2,
                                                            self.my_package.round_trip_time_server2, self.my_package.round_trip_time_server1)
                        # se printeaza rezultatul
                        print(abs(TOF))
                        time.sleep(1)
                        os._exit(0)

        except Exception as e:
            # În cazul unei erori clientul este sters
            # conexiunea cu acesta fiind închisă
            print(e)
            self.client_data.pop(client_adress)
            client_connection.close()

    # functie ce mentine conexiunea cu clientul
    def handle_connections(self, master_server):
        while True:
            # se aceptaconexiunea
            client_connection, addr = master_server.accept()
            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            # se receptioneaza datele
            t3 = threading.Thread(target=self.recive_client_data, args=(client_connection, addr,))
            t3.start()

    # functie ce receptioneaza datele de la client
    def send_data(self):
        try:
            while True:
                if len(self.client_data) > 0:
                    for client_connection in self.client_data:

                        if not self.my_package.round_trip_time_client2:
                            if self.my_package.time_recv_init:
                                self.client_data[client_connection].send(
                                    (f'{len(str(SERVER_INITIAL_SENT)):<{HEADERSIZE}}' + f'{SERVER_INITIAL_SENT}').encode())
                                self.my_package.time_send = datetime.now()
        except Exception as e:
            print("send data Something goes wrong server ", e)

    # functia principala a serverului
    def start_master_server(self, port=2997):
        try:
            #se creeaza socketul
            serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #se seteaza optiunea de refolosire a adresei, direct dupa inchiderea conexiunii
            # in caz contrar va fi necesar sa se astepte pentru o ulterioara conexiune
            serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # se asteapta realizarea conexiunii
            serversocket.bind(('', self.port))
            serversocket.listen(10)
            print("Listening...")
            t1 = threading.Thread(target=self.handle_connections, args=(serversocket,))
            t2 = threading.Thread(target=self.send_data, args=())
            t1.start()
            t2.start()
        except socket.error as err:
            print("Create Socket server  Fail with error ", err)

    # functia calculeaza TOF
    @staticmethod
    def calculate_time_of_fligth(round_trip_time_client1, tround_trip_time_client2, round_trip_time_server2, round_trip_time_server1):
        rtt1_data = datetime.strptime(round_trip_time_client1, '%H:%M:%S.%f')
        if tround_trip_time_client2 == "0:00:00":
            tround_trip_time_client2_data = datetime.strptime(tround_trip_time_client2, '%H:%M:%S')
        else:
            tround_trip_time_client2_data = datetime.strptime(tround_trip_time_client2, '%H:%M:%S.%f')

        return ((rtt1_data - tround_trip_time_client2_data) + (round_trip_time_server2 - round_trip_time_server1)) / 4

    #functie ce returneaza substringul dintre doua caractere
    @staticmethod
    def find_between(s, first, last):
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""


if __name__ == "__main__":
    package = TimePackage()
    server = Server(package)
    server.start_master_server()
