import socket

HEADERSIZE = 10

#aceasta clasa este apelata din interfata principala
# scopul de baza este de a transmite tipul algoritmului la client

class Client:
    def __init__(self, port=2998, host='localhost'):
        self.port = port
        self.host = host

    @staticmethod
    def send_data(socket_client, data_to_send):
        try:
            socket_client.send((f'{len(str(data_to_send)):<{HEADERSIZE}}' + f'{data_to_send}').encode())
        except Exception as e:
            print("Something goes wrong ", e)

    def start_clientul(self, data_to_send):
        # se creeaza socketul clientului
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # se conecteaza la server
        clientSocket.connect((self.host, self.port))
        # se trimit datele catre server
        self.send_data(clientSocket, data_to_send)
        # după ce datele au fost trimise, conexiunea este închisă
        clientSocket.close()
