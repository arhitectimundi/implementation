import tkinter as tk
from PIL import Image, ImageTk

# aceasta clasa are rolul de a afisa imaginile/GIF-urile in tkinter
class ImageLabel(tk.Label):
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master=None, cnf={}, **kw)
        self.master = master
        self.cnf = cnf
        self.frames = []

    def load(self, im):
        if isinstance(im, str):
            im = Image.open(im)
        try:
            self.frames.append(ImageTk.PhotoImage(im.copy()))
        except EOFError:
            pass
        self.config(image=self.frames[0])

    def unload(self):
        self.destroy()
