import ipaddress
import subprocess
import tkinter.ttk as tree
from pathlib import Path
from tkinter import *
from tkinter import messagebox

from PIL import ImageTk, Image

from client_send_alg_type import Client
from image_view import ImageLabel
from network import IpScanner

ALGORITHMS = [
    ("Christian algoritm", 0),
    ("Berekeley algoritm", 1),
]
COLUMNS = ("Data Calculata", "Data Reala", "Diff")

PATHS = [
    ("SavedData\\\\", "results")
]
NUM_OF_CONNECTED_DEVICES = 2


class MyGui:
    def __init__(self, window):
        # se creeaza principala fereastra a aplicatiei
        self.running_process = 0
        self.clients = {}
        self.found_devices = {}
        self.selected_algorithm = IntVar()
        self.window = window
        self.private_adress = StringVar()
        self.text_box = Entry(width=20, textvariable=self.private_adress)
        window.geometry("500x200")
        window.title("Clock synchronization algorithms")
        window.iconbitmap("Images\\Iconsmind-Outline-Clock-Forward.ico")
        window.resizable(False, False)

    def use_command(self):
        # imaginile sunt stocate ca variabile globale
        # lucru necesar cand se acestea se vor plasa intr-o fereastra copil
        global img, img2, img3
        img = ImageTk.PhotoImage(Image.open("Images\\Intro.JPEG").resize((400, 250), Image.ANTIALIAS))
        img2 = ImageTk.PhotoImage(Image.open("Images\\Rescan.JPEG").resize((400, 250), Image.ANTIALIAS))
        img3 = ImageTk.PhotoImage(Image.open("Images\\Chose.JPEG").resize((400, 250), Image.ANTIALIAS))
        # se creează o fereastră separată pentru help-ul aplicatiei
        use_window = Toplevel(self.window)
        use_window.resizable(False, False)
        # fereastra principala devine
        # indisponibila cat timp este deschisa
        # o fereastra copil
        use_window.grab_set()
        use_window.geometry("400x300")
        text1 = Text(use_window, height=400, width=300)
        text1.insert(END, "1. Initial se va introduce o adresa locala valida\n"
                          "pentru a identifica toate dispozitivele conectate")
        text1.image_create(END, image=img)
        text1.insert(END, "2. Prin intermediul butonului 'Rescaneaza' se \n"
                          "poate alege un nou domeniu de scanare,\n"
                          "sau se poate rescana domeniul introdus la punctul '1'\n")
        text1.image_create(END, image=img2)
        text1.insert(END, "3. Se alege algoritmul spre testare.\n"
                          "Dupa care este posibila analiza datelor")
        text1.image_create(END, image=img3)
        text1.pack(side=LEFT)
        text1.config(state=DISABLED)

    @staticmethod
    def show_info():
        messagebox.showinfo("Info", "Aceasta aplicatie compară 2 algoritmi de sincronizare a ceasului")

    def create_menu(self):
        my_menu = Menu(self.window)
        self.window.config(menu=my_menu)
        use_menu = Menu(my_menu, tearoff=False)
        # se creaza widget=uri cascada pentru opriunile meniurilor
        my_menu.add_cascade(label="Utilizare", menu=use_menu)
        use_menu.add_command(label="Ghid de utilizare", command=self.use_command)
        info_menu = Menu(my_menu, tearoff=False)
        my_menu.add_cascade(label="Despre", menu=info_menu)
        info_menu.add_command(label="Despre Aplicatie", command=self.show_info)

    # aceasta functie cauta toate device-urile din domeniu selectat cu masca /24
    def find_devices(self):
        label_private_address = Label(self.window, text="Introduce-ti adresa privata(ex. 192.168.0.1) ")
        label_private_address.pack()
        self.text_box.pack()
        label = Label(self.window, text="Caută dispozitive conectate")
        label.pack()
        button = Button(self.window, text="Caută dispozitive",
                        command=lambda: ([self.text_box.pack_forget(), label_private_address.destroy(),
                                          self.find_rescan(button, label)])
                        if self.validate_ip_adress(self.private_adress.get())
                        else (self.validate_private_adress_entry()))
        button.pack()

    def find_rescan(self, button, label, path="Images\\200.gif"):
        # listbox pentru dispozitivele gasite conectate
        itemlist = Listbox(self.window, selectmode="multiple", width=75)
        scrollbar = Scrollbar(self.window)
        image = ImageLabel(self.window)
        image.pack()
        image.load(path)
        label.config(text="Se cauta dispozitivele!")
        button.after(1000, lambda: [self.get_devices(itemlist, scrollbar), image.unload(),
                                    label.config(text="Alege dispozitive pentru conectare:")])
        button.pack_forget()
        top = Frame(self.window)
        top.pack(side=TOP)
        # buton pentru rescanarea domeniului
        retry_btn = Button(self.window, text="Rescanează!",
                           command=lambda: self.rescan_domain(itemlist, scrollbar, button, find_btn, retry_btn,
                                                              label, top))
        # buton pentru scanarea  deviceurilor selectate
        find_btn = Button(self.window, text="Conecteaza-te",
                          command=lambda:
                          (self.remove_unnecessary_widget(button, find_btn, retry_btn, label, scrollbar, top))
                          # aceasta conditie verifica ca macar un client sa fie conectat
                          if self.connect_to_devices(button, label, itemlist)
                          else None)
        retry_btn.pack(in_=top, side=LEFT)
        find_btn.pack(in_=top, side=LEFT)

    # metoda statica ce valideaza o adresa IP
    @staticmethod
    def validate_ip_adress(adress):
        if not adress:
            return False
        else:
            local_address = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})').search(adress)
            try:
                if not local_address:
                    return False
                # se verifica ca adresa sa fie din domeniul privat(local)
                if ipaddress.ip_address(local_address.group()).is_private:
                    return True, local_address.group()
                else:
                    return False
            except ValueError:
                return False

    # functie ce are scop de atentionarea in cazul introducerii unei adrese IP invalide
    def validate_private_adress_entry(self):
        self.text_box.config(background="red")
        self.text_box.delete(0, END)
        messagebox.showwarning("Atentie", "Introduceti o adresa(IP) privata valida")
        self.text_box.config(background="white")

    # functie de rescanare a domeniului
    def rescan_domain(self, itemlist, scrollbar, button, find_btn, retry_btn, label, top):
        # se ofera optiunea de a scana un nou domeniu sau de a rescana deviceurile pe acelasi domeniu
        if messagebox.askyesno("Rescanează", "Introduce-ți un nou domeniu?"):
            self.remove_unnecessary_widget(button, find_btn,
                                           retry_btn, label, scrollbar, top)
            itemlist.destroy()
            self.find_devices()
        else:
            self.get_devices(itemlist, scrollbar)

    @staticmethod
    def remove_unnecessary_widget(button, find_btn, retry_btn, label, scrollbar, top):
        button.destroy()
        find_btn.destroy()
        retry_btn.destroy()
        label.pack_forget()
        scrollbar.destroy()
        top.destroy()

    # functie necesara pentru obtinerea deviceurilor conectate
    def get_devices(self, itemlist, scrollbar):
        if not itemlist.get(0, END):
            itemlist.pack(side=LEFT, fill=BOTH)
            scrollbar.pack(side=RIGHT, fill=BOTH)
            itemlist.config(yscrollcommand=scrollbar.set)
            scrollbar.config(command=itemlist.yview)

        self.found_devices = IpScanner(self.private_adress.get()).scan_adress()
        itemlist.delete(0, END)
        # inserarea in listbox a adreselor si numele hostului (fara o rezolvare DNS)
        for key in self.found_devices:
            itemlist.insert(END, "IP: " + key + " HostName: " + self.found_devices[key])

    # conectarea la deviceurile selectate
    # value_repeat se foloseste ca flag pentru a decide daca device-urile
    # la care se conecteaza trebuie rescanate, sau trebuie afisate ca rezultatul unui buton de 'BACK'
    def connect_to_devices(self, button, label, itemlist=None, value_repeat=0):
        if not value_repeat:
            self.clients = {}
            self.selected_algorithm.set(0)
            # citesc valorile selectate in listbox
            values = [itemlist.get(idx) for idx in itemlist.curselection()]
            if values:
                for value in values:
                    # adresele scanate se verifica pentru a fi din domeniul privat(local)
                    is_valid, val = self.validate_ip_adress(value)
                    if is_valid:
                        try:
                            # fiecare device selectat este instantiat ca un client TCP
                            # pentru a transmite ulterior algoritmul selectat de user
                            # la device
                            self.clients[val] = Client(host=val)
                        except Exception:
                            print("Unable to creat client")

        # pentru clientii conectati se alege algoritmul de rulare
        if self.clients:
            radio_btn = list()
            if not value_repeat:
                itemlist.destroy()
            label1 = Label(self.window, text="Selectați Algoritmul Dorit:\n Colectarea datelor se efectueaza "
                                             "prin 4 apasari ale butonului(hardware) ")
            label1.pack(side=TOP)
            for val, text in enumerate(ALGORITHMS):
                radio_btn.append(Radiobutton(self.window, text=text[0], variable=self.selected_algorithm, value=val,
                                             tristateval=5))
                radio_btn[val].pack(anchor=NW, side=TOP)

            bottom = Frame(root)
            bottom.pack(side=BOTTOM, fill=BOTH, expand=True)
            # buton pentru a rula algoritmul
            select_algorithm_btn = Button(self.window, text="Rulează algoritm",
                                          command=lambda: [select_algorithm_btn.destroy(), label1.pack_forget(),
                                                           back_to_scan_btn.destroy(),
                                                           self.send_data_client(button, label, label1, radio_btn),
                                                           bottom.destroy()
                                                           ])
            # button pentru a rescana device-urile inca odata
            back_to_scan_btn = Button(self.window, text="Revezi device-urile",
                                      command=lambda: [select_algorithm_btn.destroy(), label1.destroy(),
                                                       back_to_scan_btn.destroy(), self.clear_radio_btn(radio_btn),
                                                       self.find_rescan(button, label),
                                                       bottom.destroy()])
            select_algorithm_btn.pack(in_=bottom, side=RIGHT)
            back_to_scan_btn.pack(in_=bottom, side=RIGHT)
            return True
        else:
            messagebox.showwarning("Atentie!", "Selectați măcar un dispozitiv pentru conectare")
            return False

    @staticmethod
    def clear_radio_btn(radio_btn_list):
        for radio_button in radio_btn_list:
            radio_button.destroy()

    # functie pentru a transmite data la client
    def send_data_client(self, button, label, label1, radio_btn, path="Images\\200.gif"):
        tree_view = tree.Treeview(self.window, columns=COLUMNS)
        label1.config(text="Rezultatele algoritmului " + ALGORITHMS[self.selected_algorithm.get()][0])
        label1.pack()
        image = ImageLabel(self.window)
        client_con_error = 0
        image.pack()
        image.load(path)
        # button pentru a reveni la clientii cu o ulterioara rescanare
        my_button = Button(self.window, text="Înapoi",
                           command=lambda: [label1.pack_forget(), tree_view.pack_forget(), my_button.pack_forget(),
                                            self.connect_to_devices(button, label, value_repeat=1)])
        my_button.pack(side=TOP, anchor=N)
        for key, value in list(self.clients.items()):
            self.clear_radio_btn(radio_btn)
            for valoare in self.clients.keys():
                print("Valoare " + str(valoare))
            try:
                # pentru clientii selectati se creaza un socket
                # si se transmite la dispozitive ce algoritm trebuie rulat
                self.clients[key].start_clientul(self.selected_algorithm.get())
            except (ConnectionError, ConnectionRefusedError, TimeoutError):
                messagebox.showerror("Eroare", "Probabil dispozitivul selectat:\n" + key +
                                     "\nnu a accepta conexiunea.\n")

                del self.clients[key]
                # daca conexiunea nu se acepta, marcam acest lucru
                client_con_error += 1
                # cand nu se poate conecta la nici un device selectat se rescaneaza domeniul
                if not len(self.clients):
                    messagebox.showinfo("Invalid devices", "Nu se poate conecta la nici un device\n"
                                                           "Se rescaneaza domeniul")
                    image.unload()
                    my_button.destroy()
                    label1.pack_forget()
                    label.pack(side=TOP, anchor=S)
                    self.find_rescan(button, label)

        # daca s-a reusit sa se conecteze la un client, se va rula algoritmul selectat
        if len(self.clients):
            image.unload()
            self.choose_server_algorithm(self.selected_algorithm.get())
            # se afiseaza rezultatele algoritmului sub forma unui treeview
            self.create_view_algorithm(tree_view)

    # se alege algoritmul ce va fi rulat
    @staticmethod
    def choose_server_algorithm(selected_algorithm):
        return {
            # se foloseste proces pentru a nu inchide interfata(procesul actual) atunci cand se inchide(serverul)
            subprocess.Popen("python PythonChristian\\server_christian.py", stdout=subprocess.PIPE).communicate(): 0,
            'a': 1  # ...urmeaza sa fie adaugata
        }.get(selected_algorithm, None)

    # se creeaza treeviewul pentru a reprezenta datele rularii
    def create_view_algorithm(self, tree_view):
        tree_view.pack(side=TOP, fill=X)
        # pentru fiecare coloana se stabilesc dimensiunile
        tree_view.column("Data Calculata", width=50, stretch=True)
        tree_view.column("Data Reala", width=50, stretch=True)
        tree_view.column("Diff", width=50, stretch=True)

        # se definesc hederele pentru coloane
        tree_view.heading("Data Calculata", text="Data Calculat", anchor=W)
        tree_view.heading("Data Reala", text="Data Reala", anchor=W)
        tree_view.heading("Diff", text="Diff", anchor=W)
        # afisarea rezultatelor in urma rularii algoritmului
        self.insert_data_tree(tree_view)

    # se insereaza datele in treeviewul creat
    def insert_data_tree(self, tree_view):
        row_names = []
        iterator = 0
        for key, value in self.clients.items():
            if key in self.found_devices.keys():
                data = self.read_data_from_file(PATHS[0][0] + key + ".txt")
                if data:
                    splited_data = data[0].split(",")
                    row_names.append("row" + str(key))
                    print(splited_data)
                    if len(splited_data) == 3:

                        for _, name in enumerate(row_names):
                            if iterator < NUM_OF_CONNECTED_DEVICES:
                                name = tree_view.insert("", END, text=self.found_devices[key],
                                                        values=(splited_data[0], splited_data[1], splited_data[2]))
                                iterator += 1
                                for idx in range(1, len(data)):
                                    tree_view.insert(name, END, values=(data[idx].split(",")))

                else:
                    messagebox.showwarning("Timeout", str(key) + " nu a intors nici un rezultat, reincercati")

    # se citesc datele din fisier, cu o ulterioara trunchiere dupa
    @staticmethod
    def read_data_from_file(file_name):
        myFile = Path(file_name)
        if myFile.is_file():
            with open(file_name, 'r+') as file:
                lines = file.readlines()
                file.truncate(0)
                return lines


if __name__ == '__main__':
    root = Tk()
    my_gui = MyGui(root)
    my_gui.create_menu()
    my_gui.find_devices()
    root.mainloop()
