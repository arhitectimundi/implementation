import nmap
import socket

# aceasca clasa scaneaza un anumit domeniu ip
# returneaza un dictionar cu cheia = IP si valoare = numele hostului(nerezolvat DNS)
class IpScanner(object):
    def __init__(self, ip):
        self.ip = ip

    def scan_adress(self):
        dictionary = {}
        if len(self.ip) == 0:
            my_adress = '192.168.1.1/24'
        else:
            my_adress = self.ip + '/24'
        networkmap = nmap.PortScanner()

        # ping scan disable activate port scan
        networkmap.scan(hosts=my_adress, arguments='-sn')
        for host in networkmap.all_hosts():
            dictionary[host] = socket.getfqdn(host)
        return dictionary
