import socket
import datetime
import time
from dataclasses import dataclass
import threading
from dateutil import parser

@dataclass
class Time_Package:
    time_send_init : time = 0
    time_recv_init : time = 0
    time_send_final: time = 0
    
class Client:
    def __init__(self,my_package,port = 2997, host = 'localhost'):
        self.host = host
        self.port = port
        self.my_package = my_package

#function to send time to the server
    def send_time(self,client_socket):
        flag = True
        while True:
            if self.my_package.time_send_init == 0 :
                self.my_package.time_send_init =datetime.datetime.now()
                client_socket.send(str("Csi").encode())
                print("Client- Am trimis primul timp")
            elif self.my_package.time_recv_init != 0  and flag == True:
                self.my_package.time_send_final  = datetime.datetime.now()
                client_socket.send(str("Csf").encode())
                flag = False
                print("Client- am trimis al doilea timp")
            else :
                round_trip_time1 = abs(self.my_package.time_recv_init - self.my_package.time_send_init)
                reply2 = abs(self.my_package.time_send_final - self.my_package.time_recv_init)
                client_socket.send("i1".encode()  + str(round_trip_time1).encode() + "f1".encode())
                client_socket.send("i2".encode() + str(reply2).encode() + "f2".encode())
                print("Am trimis datele finale")
                self.my_package.time_send_init = 0
                self.my_package.time_send_final  = 0
                flag = True
                
                
                
            time.sleep(1)

#function for reciving server time :
    def recive_time(self,client_socket):
        while True:
            recive_date = client_socket.recv(1024).decode()
            if "Ssi" in recive_date:
                recive_date = recive_date.replace('Ssi','')
                self.my_package.time_recv_init = datetime.datetime.now()
                print ("Am primit data de la server")
#main function of class
    def client_connection(self):
        try:
            #create client socket
            clientSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            #connect to server
            clientSocket.connect((self.host,self.port))
            threading.Thread(target=self.send_time,args = (clientSocket, )).start()
            threading.Thread(target=self.recive_time,args=(clientSocket, )).start()
        except Exception as e:
            print("Something goes wrong client : ",e)


if __name__ == "__main__":
    my_pack = Time_Package()
    client_server = Client(my_pack)
    client_server.client_connection()