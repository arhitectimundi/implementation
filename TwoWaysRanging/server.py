import socket
import threading
from dataclasses import dataclass
import socket
import threading
from datetime import timedelta,datetime
import sys
import time
from dateutil import parser

@dataclass
class Time_Package:
    time_recv_init : datetime = 0
    time_send: datetime = 0
    time_recv_final: datetime = 0
    rtt_1 : datetime = 0
    r1 : datetime = 0
    r2 : datetime = 0
    rtt_2 : datetime = 0
    


class Server:
#constructor of Server
    def __init__(self,my_package,port=2997,client_data = {}):
        self.client_data = client_data
        self.port = port
        self.my_package = my_package



#recive the data from the clients and save in a dictionary where key = client_adress,
# data =  client_time,difference between client and master time,and client connection
    def recive_client_data(self,client_connection,client_adress):
        self.client_data[client_adress] = client_connection
        while True:
            recive_data = client_connection.recv(1024).decode()
            self.client_data[client_adress] = {
                "myPackage" : self.my_package,
                "client connection ": client_connection
                }
            print(recive_data)
            if recive_data == "Csi" :
                self.my_package.time_recv_init = datetime.now()
                print("Am primit prima data")
            elif recive_data == 'Csf':
                self.my_package.time_recv_final = datetime.now()
                print("Am primit ultima data")
            else:
                self.my_package.rtt_1 = self.find_between(recive_data,"i1","f1")
                self.my_package.r1 = self.find_between(recive_data,"i2","f2")
                if self.my_package.time_send != 0:
                    self.my_package.rtt_2 = abs(self.my_package.time_recv_final - self.my_package.time_send)
                    self.my_package.r2 = abs(self.my_package.time_send - self.my_package.time_recv_init)
                    print("rtt_1 = ", self.my_package.rtt_1)
                    print("r1 = ", self.my_package.r1)
                    print("rtt_2 = ", self.my_package.rtt_2)
                    print("r2 = ", self.my_package.r2)
                    TOF = self.calculate_time_of_fligth(self.my_package.rtt_1,self.my_package.r1,  self.my_package.rtt_2,self.my_package.r2)
                    print ("TOF = " ,abs(TOF))
                    
                    distance  = timedelta.total_seconds(TOF)*300000000
                    print("Distance = ", distance)

                    

            time.sleep(1)
            

#function to handle connection of server to master
    def handle_connections(self, master_server):
        while True:
            client_connection, addr = master_server.accept()
            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            threading.Thread(target=self.recive_client_data,args = (client_connection,addr, )).start()


    
#syncronize the client clocks
    def send_data(self):
        while True:
            if len(self.client_data) > 0:
                for _,client_connection in self.client_data.items():
                    try:
                        if self.my_package.r1 == 0 :
                            if self.my_package.time_recv_init != 0 :
                                client_connection["client connection "].send(str("Ssi").encode())
                                self.my_package.time_send = datetime.now()
                                print("Am trimis data mea")
                                self.my_package.r1  = 0
                    except Exception as e:
                        print("Something goes wrong server " , e)
            else:
                print("No data from client")
                print("\n\n")
        
            time.sleep(1)
#main function of a server
    def start_master_server(self,port = 2997):
        try:
            serversocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            serversocket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
            serversocket.bind(('',self.port))
            serversocket.listen(10) 
            print("Listening...")
            threading.Thread(target = self.handle_connections, args = (serversocket, )).start()
            threading.Thread(target= self.send_data, args =()).start()
        except socket.error as err:
            print ("Create Socket server  Fail with error ", err)
            sys.exit(1)
    def calculate_time_of_fligth(self,rtt1,tr1,rtt2,tr2):
        rtt1_data = datetime.strptime(rtt1, '%H:%M:%S.%f')
        tr1_data = datetime.strptime(tr1,'%H:%M:%S.%f')
        return ((rtt1_data-tr1_data) + (rtt2-tr2))/4

    def find_between(self, s, first, last ):
        try:
            start = s.index( first ) + len( first )
            end = s.index( last, start )
            return s[start:end]
        except ValueError:
            return ""

if __name__ == "__main__":
    package = Time_Package()
    server = Server(package)
    server.start_master_server()