import socket 
import threading
import time
from datetime import datetime,timedelta
from dateutil import parser
from timeit import default_timer 



class Client:
    def __init__(self,port = 2997, host = 'localhost'):
        self.port  = port
        self.host = host
    def send_data(self,client_server):
               time.sleep(10)
               client_server.send(str('E').encode())

    def recive_data(self,client_server):
        while True:
            #Define a default timer, in a platform-specific manner. On Windows, time.clock() has microsecond granularity
            time_of_request = default_timer()
            #recive time from server
            server_time = parser.parse(client_server.recv(1024).decode())
            #calculate time of reciving response
            time_of_response = default_timer()
            #calculate actual time
            actual_client_time = datetime.now()
            round_trip_time  = time_of_response - time_of_request
            #calculate normal clock time
            calculated_client_time = server_time + timedelta(minutes=(round_trip_time)/2)
            #error
            error  = abs(actual_client_time - calculated_client_time)
            
            print("Server_Time ", server_time)
            print("Round_Trip_Time", round_trip_time)
            print("Real Client time ",actual_client_time)
            print("Calculated_client_time ",calculated_client_time)
            print("Error betwween server and client is ",error)
            print("\n\n")
            time.sleep(5)
            
    def start_client(self):
        try:
            #create client socket
            clientSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            #connect to server
            clientSocket.connect((self.host,self.port))
            threading.Thread(target=self.recive_data,args=(clientSocket, )).start()
            threading.Thread(target=self.send_data, args = (clientSocket, )).start()

        except socket.error as err:
            print("Create Socket client fail with error ",err)


if __name__ == "__main__":
    my_client = Client()
    my_client.start_client()