import socket
import time
from datetime import datetime
import threading
import sys
from dateutil import parser

class Server:

    def __init__(self,connected_list = {},port= 2997):
        self.connected_list = connected_list
        self.port = port

    def start(self):

        try:
            serversocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            serversocket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
            serversocket.bind(('',self.port))
            serversocket.listen(10) 
            print("Listening...")
            threading.Thread(target=self.handle_connection,args=(serversocket, )).start()
            threading.Thread(target= self.send_clock_to_client,args = ()).start()
        except socket.error as err:
            print ("Create Socket server  Fail with error ", err)
            sys.exit(1)
        
#method that saves the data about a connected client  
    def recive_client_data(self,client_connection,client_adress):
        while True:
            self.connected_list[client_adress] = client_connection
            client_data = client_connection.recv(1024).decode()
            print("Client data ",client_data)
    
            if client_data == "E" :
                try:
                    self.connected_list.pop(client_adress)
                except Exception as err:
                    print("Some error ", err)
                    sys.exit(1)
        time.sleep(5)


#handle method for connection with server
    def handle_connection(self,server_connection):
        while True:
            client_connection,addr = server_connection.accept()
            print("Connected to a client with ip " + str(addr[0]) + "and port " + str(addr[1]))
            threading.Thread(target=self.recive_client_data,args = (client_connection,addr, )).start()


    def send_clock_to_client(self):
        while True:
            if len(self.connected_list) > 0 :
                for client_connection in self.connected_list:
                    try:
                        self.connected_list[client_connection].sendall(str(datetime.now()).encode())
                    except Exception as e:
                        print ("Something goes wrong ", e)
            else : 
                print("No clients found")
            time.sleep(5)



if __name__ == "__main__":
    myServer = Server()
    myServer.start()
